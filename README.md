# README #

This is set of small library routines for various usage.
Routines are divided into generic and platform specific.
The project is at the development stage.

POSIX routines:

* fd_pair - smart wrapper of two file descriptors. Normally use for a pipe or socketpair descriptors;

* shared_lib - wrapper for a dlopen\dlsym calls with some additional checks;

* unnamed_pipe - creating function for an unnamed pipe;

* fd_blocking_mode - functions for (un)setting a blocking mode on file descriptor;

* process_runner - complex class for running application in safe environment, for example, preventing file descriptors inheritance or multithreaded state inheritance.

Generic routines:

* low_level_buffer - POD container, that encapsulate sending/receiving buffer. It eliminates the need to create a packaged structure (with alignment 1) giving easy access to parts of the buffer, as if it were the structure fields, and provides some compile-time and run-time checks. Dynamic fields (with changeable length) and network byte order are also supported;

* safe_bool - safe_bool idiom implementation;

* noncopyable - noncopyable idiom implementation;

* move - move idiom implementation for C++98|03;

* align_as - determination of alignment for C++98|03;

* string_view - string view idiom for C++98|03;

* endian_reverse - set of template functions that reverse memory according with network byte order.