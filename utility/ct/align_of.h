#ifndef UTILITY_CT_ALIGN_OF_H_INCLUDED_
#define UTILITY_CT_ALIGN_OF_H_INCLUDED_

#include <cstddef>
#include "integral_constant.h"

namespace ct
{

namespace detail
{

template <typename T>
struct big
{
    T x; char c;
};

template <typename T>
struct big<T[]> : big<T>
{ };

} // namespace detail

template <typename T, typename Big = detail::big<T> >
struct align_of;

namespace detail
{

template <typename T, size_t Diff>
struct helper
    : integral_constant<size_t, Diff>
{ };

template <typename T>
struct helper<T, 0>
    : align_of<T>
{ };

} // namespace detail

template <typename T, typename Big>
struct align_of
    : detail::helper<Big, (sizeof(Big) - sizeof(T))>
{ };

template <typename T, typename Big>
struct align_of<T[], Big>
    : align_of<T>
{ };

} // namespace ct

#endif // UTILITY_CT_ALIGN_OF_H_INCLUDED_
