#ifndef CT_TYPE_LIST_DETAIL_H_INCLUDED
#define CT_TYPE_LIST_DETAIL_H_INCLUDED

namespace ct
{

#if __cplusplus >= 201103L // C++11

template <typename ...Args>
struct type_list;

template <typename ...Args>
struct make_list
{
    using type = type_list<Args...>;
};

typedef type_list<> null_list;

#else  // C++98

struct null_list
{ };

template <typename TailT, typename HeadT = null_list>
struct type_list
{
    typedef HeadT head_type;
    typedef TailT tail_type;
};

template <
    typename Arg00 = null_list, typename Arg01 = null_list
  , typename Arg02 = null_list, typename Arg03 = null_list
  , typename Arg04 = null_list, typename Arg05 = null_list
  , typename Arg06 = null_list, typename Arg07 = null_list
  , typename Arg08 = null_list, typename Arg09 = null_list
  , typename Arg10 = null_list, typename Arg11 = null_list
  , typename Arg12 = null_list, typename Arg13 = null_list
  , typename Arg14 = null_list, typename Arg15 = null_list
  , typename Arg16 = null_list, typename Arg17 = null_list
  , typename Arg18 = null_list, typename Arg19 = null_list
  , typename Arg20 = null_list
>
struct make_list
{
private:
    typedef typename make_list<
        Arg01, Arg02, Arg03, Arg04, Arg05, Arg06
      , Arg07, Arg08, Arg09, Arg10, Arg11, Arg12
      , Arg13, Arg14, Arg15, Arg16, Arg17, Arg18
      , Arg19, Arg20
    >::type tail_type;

public:
    typedef type_list<Arg00, tail_type> type;
};

template <>
struct make_list<>
{
    typedef null_list type;
};

#endif

} // namespace ct

#endif // CT_TYPE_LIST_DETAIL_H_INCLUDED
