#ifndef UTILITY_CT_ELEMENTS_COUNT_H_INCLUDED_
#define UTILITY_CT_ELEMENTS_COUNT_H_INCLUDED_

namespace ct
{

namespace detail
{

template <typename T, size_t N>
char (& static_elements_count(T const (&)[N]))[N];

template <typename T>
char (& static_elements_count(T const &))[1];

} // namespace detail

#define UTILITY_ELEMENTS_COUNT(Arr) \
    (sizeof(::ct::detail::static_elements_count((Arr))))

} // namespace ct

#endif // UTILITY_CT_ELEMENTS_COUNT_H_INCLUDED_
