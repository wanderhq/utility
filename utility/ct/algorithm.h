#ifndef CT_MPL_ALGORITHM_H_INCLUDED
#define CT_MPL_ALGORITHM_H_INCLUDED

namespace ct
{

template <size_t A, size_t B>
struct min
{
    enum { value = A < B ? A : B };
};

template <size_t A, size_t B>
struct max
{
    enum { value = A > B ? A : B };
};

} //namespace ct

#endif // CT_MPL_ALGORITHM_H_INCLUDED

