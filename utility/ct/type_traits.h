#ifndef TYPE_TRAITS_H_INCLUDED_
#define TYPE_TRAITS_H_INCLUDED_

#include "logical.h"
#include "declval.h"

#include "sfinae_types.h"

#include <cstddef>

namespace ct
{

// const-volatile modifications.
// remove_const
template <typename Tp>
struct remove_const
{
    typedef Tp     type;
};
template <typename Tp>
struct remove_const<Tp const>
{
    typedef Tp     type;
};

// remove_volatile
template <typename Tp>
struct remove_volatile
{
    typedef Tp     type;
};
template <typename Tp>
struct remove_volatile<Tp volatile>
{
    typedef Tp     type;
};

// remove_cv
template <typename Tp>
struct remove_cv
    : remove_const<typename remove_volatile<Tp>::type>
{ };

// add_const
template <typename Tp>
struct add_const
{
    typedef Tp const     type;
};

// add_volatile
template <typename Tp>
struct add_volatile
{
    typedef Tp volatile  type;
};

// add_cv
template <typename Tp>
struct add_cv
    : add_const<typename add_volatile<Tp>::type>
{ };

// is_void
template <typename Tp>
struct is_void
    : false_type
{ };
template <>
struct is_void<void>
    : true_type
{ };
template <>
struct is_void<void const>
    : true_type
{ };

// is_array
template <typename Tp>
struct is_array
    : false_type
{ };
template <typename Tp, std::size_t Size>
struct is_array<Tp[Size]>
    : true_type
{ };
template <typename Tp>
struct is_array<Tp[]>
    : true_type
{ };

template <typename Tp>
struct is_array_of_unknown_bound
    : false_type
{ };
template <typename Tp>
struct is_array_of_unknown_bound<Tp[]>
    : true_type
{ };

// is_same
template <typename Tp1, typename Tp2>
struct is_same
    : false_type
{ };
template <typename Tp1>
struct is_same<Tp1, Tp1>
    : true_type
{ };

// is_class
namespace detail
{
    template <typename Tp>
    struct is_class_helper
    {
    private:
        template <typename C>
        static yes_type is_class_check(int C:: *);
        template <typename C>
        static no_type  is_class_check(...);

    public:
        enum
        {
            value = sizeof( is_class_check<Tp>(0) ) - 1
        };
    };
}  // detail
template <typename Tp>
struct is_class
    : integral_constant<bool, detail::is_class_helper<Tp>::value>
{ };

// is_pointer
namespace detail
{
    template <typename Tp>
    struct is_pointer_helper
        : false_type
    { };

    template <typename Tp>
    struct is_pointer_helper<Tp *>
        : true_type
    { };
} // detail
template <typename Tp>
struct is_pointer
    : detail::is_pointer_helper<typename remove_cv<Tp>::type>::type
{ };

// is_reference
template <typename Tp>
struct is_lvalue_reference
    : false_type
{ };
template <typename Tp>
struct is_lvalue_reference<Tp &>
    : true_type
{ };

template <typename Tp>
struct is_rvalue_reference
    : false_type
{ };
#if __cplusplus >= 201103L
template <typename Tp>
struct is_rvalue_reference<Tp &&>
    : true_type
{ };
#endif

template <typename Tp>
struct is_reference
    : or_< is_lvalue_reference<Tp>, is_rvalue_reference<Tp> >::type
{ };

// is_convertible
namespace detail
{
    template <typename Tp1, typename Tp2>
    struct is_convertible_helper
    {
    private:
        template <typename U>
        static yes_type is_convertible_check(U const &);
        template <typename U>
        static no_type  is_convertible_check(...);

    public:
        enum
        {
            value = sizeof( is_convertible_check<Tp2>(declval<Tp1>()) ) - 1
        };
    };
    template <typename Tp>
    struct is_convertible_helper<Tp, Tp>
    {
    private:
        template <typename U>
        static yes_type is_convertible_check(U);
        template <typename U>
        static no_type  is_convertible_check(...);

    public:
        enum
        {
            value = sizeof( is_convertible_check<Tp>(declval<Tp>()) ) - 1
        };
    };
} // detail
template <typename Tp1, typename Tp2>
struct is_convertible
    : integral_constant<bool, detail::is_convertible_helper<Tp1, Tp2>::value>
{ };

// reference modifications
// remove_reference
template <typename Tp>
struct remove_reference
{
    typedef Tp   type;
};
template <typename Tp>
struct remove_reference<Tp &>
{
    typedef Tp   type;
};
#if __cplusplus >= 201103L
template <typename Tp>
struct remove_reference<Tp &&>
{
    typedef Tp   type;
};
#endif

// add_reference
// add_lvalue_reference
namespace detail
{
    template <typename Tp, bool IsRefOrVoid, bool IsRvRef>
    struct add_lvalue_reference_helper
    {
        typedef Tp   type;
    };
    template <typename Tp>
    struct add_lvalue_reference_helper<Tp, false, false>
    {
        typedef Tp & type;
    };
    template<typename Tp>
    struct add_lvalue_reference_helper<Tp, true, true>
    {
        typedef typename remove_reference<Tp>::type & type;
    };
} // detail
template <typename Tp>
struct add_lvalue_reference
    : detail::add_lvalue_reference_helper<
          Tp
        , is_reference<Tp>::value || is_void<Tp>::value
        , is_rvalue_reference<Tp>::value
      >
{ };

// add_rvalue_reference
namespace detail
{
    template <typename Tp, bool IsRefOrVoid>
    struct add_rvalue_reference_helper
    {
        typedef Tp     type;
    };
#if __cplusplus >= 201103L
    template <typename Tp>
    struct add_rvalue_reference_helper<Tp, false>
    {
        typedef Tp &&  type;
    };
#endif
} // detail
template <typename Tp>
struct add_rvalue_reference
    : detail::add_rvalue_reference_helper<
          Tp
        , is_reference<Tp>::value || is_void<Tp>::value
      >
{ };

template <typename Tp>
struct add_reference
    : detail::add_rvalue_reference_helper<
          Tp
        , is_reference<Tp>::value || is_void<Tp>::value
      >
{ };

// pointer modifications
// remove_pointer
namespace detail
{
    template <typename Tp, typename Up>
    struct remove_pointer_helper
    {
        typedef Tp     type;
    };
    template <typename Tp, typename Up>
    struct remove_pointer_helper<Tp, Up *>
    {
        typedef Up     type;
    };
} // detail
template <typename Tp>
struct remove_pointer
    : detail::remove_pointer_helper<Tp, typename remove_cv<Tp>::type>
{ };

// add_pointer
template <typename Tp>
struct add_pointer
{
    typedef typename remove_reference<Tp>::type * type;
};

// is_function
namespace detail
{
    template <typename Tp>
    struct is_function_helper
    {
    private:
        template <typename F>
        static ct::yes_type check_is_function( ... );
        template <typename F>
        static ct::no_type  check_is_function( F (*)[1] );

    public:
        enum
        {
            value = !is_class<Tp>::value
                 && !is_void<Tp>::value
                 && !is_reference<Tp>::value
                 && !is_array<Tp>::value
                 && (sizeof( check_is_function<Tp>(0) ) - 1)
        };
    };
} // detail
template <typename Tp>
struct is_function
    : integral_constant<bool, detail::is_function_helper<Tp>::value>
{ };

// is_empty
namespace detail
{
    template <typename T>
    struct empty_helper_t1
        : public T
    {
        empty_helper_t1();  // hh compiler bug workaround
        int i[256];
    };
    struct empty_helper_t2
    { int i[256]; };

    template <typename T, bool is_a_class = false>
    struct empty_helper
        : ct::false_type
    { };

    template <typename T>
    struct empty_helper<T, true>
        : ct::integral_constant<bool, sizeof(empty_helper_t1<T>) == sizeof(empty_helper_t2)>
    { };
} // detail

template <typename T>
struct is_empty
    : detail::empty_helper<typename remove_cv<T>::type, is_class<T>::value>
{ };


// is_base_of
namespace detail
{
    template <typename B, typename D>
    struct is_base_of_helper
    {
        struct host
        {
            operator B *() const;
            operator D *();
        };

        template <typename T>
        static ct::yes_type check_is_base_of(D *, T);
        static ct::no_type  check_is_base_of(B *, int);

        enum
        {
            value = sizeof(check_is_base_of(host(), int())) - 1
        };
    };
} // detail

template <typename Bp, typename Dp>
struct is_base_of
    : integral_constant<bool, detail::is_base_of_helper<Bp, Dp>::value>
{ };

// is_complete
namespace detail
{

#if (__GNUC__ == 4 && __GNUC_MINOR__ < 6)
template <typename T>
struct is_complete
    : integral_constant<bool
         , !is_void<T>::value
        && !is_function<T>::value
        && !is_array_of_unknown_bound<T>::value
      >
{ };

#else

template <typename T>
struct is_complete
{
    template <typename U, size_t L = sizeof(U)>
    struct check
    {
        typedef U type;
    };
    template <typename U>
    static no_type  check_is_complete(...);

    template <typename U>
    static yes_type check_is_complete(typename check<U>::type *);
    enum
    {
        value = !is_function<T>::value && ( sizeof(check_is_complete<T>(0)) - 1 )
    };
};

#endif
} //detail

template <typename Tp>
struct is_complete
    : integral_constant<bool, detail::is_complete<Tp>::value>
{ };

// remove_extent
template <typename Tp>
struct remove_extent
{
    typedef Tp type;
};

template <typename Tp>
struct remove_extent<Tp[]>
{
    typedef Tp type;
};

template <typename Tp, size_t N>
struct remove_extent<Tp[N]>
{
    typedef Tp type;
};

// remove_all_extents
template <typename Tp>
struct remove_all_extents
{
    typedef Tp type;
};

template <typename Tp>
struct remove_all_extents<Tp[]>
    : remove_all_extents<Tp>
{ };

template <typename Tp, size_t N>
struct remove_all_extents<Tp[N]>
    : remove_all_extents<Tp>
{ };

} // ct

#endif // TYPE_TRAITS_H_INCLUDED_
