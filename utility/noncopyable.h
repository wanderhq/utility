#ifndef UTILITY_NONCOPYABLE_H_INCLUDED_
#define UTILITY_NONCOPYABLE_H_INCLUDED_

namespace utility
{

class noncopyable
{
protected:
    noncopyable() throw()
    {}
    ~noncopyable() throw()
    {}

private: // emphasize the following members are private
    noncopyable(noncopyable const &);
    noncopyable & operator=(noncopyable const &);
};

} // namespace utility

#endif // UTILITY_NONCOPYABLE_H_INCLUDED_
