#ifndef UTILITY_STRING_VIEW_H_INCLUDED_
#define UTILITY_STRING_VIEW_H_INCLUDED_

#include <iterator>
#include <string>

namespace utility
{

template <typename CharT, typename TraitsT = std::char_traits<CharT> >
class basic_string_view
{
public:
    typedef CharT const *       const_iterator;
    typedef const_iterator      iterator;

    typedef TraitsT                      traits_type;
    typedef typename TraitsT::char_type  value_type;

    typedef std::size_t         size_type;
    typedef std::ptrdiff_t      difference_type;
    typedef value_type &        reference;
    typedef value_type const &  const_reference;
    typedef value_type *        pointer;
    typedef value_type const *  const_pointer;

    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
    typedef const_reverse_iterator                reverse_iterator;

    static size_type const npos = size_type(-1);

public:
    basic_string_view() throw()
        : m_begin(""), m_end(m_begin)
    { }
    basic_string_view(basic_string_view const & x) throw()
        : m_begin(x.m_begin), m_end(x.m_end)
    { }
    basic_string_view & operator=(basic_string_view const & x) throw()
    {
        m_begin = x.m_begin;
        m_end   = x.m_end;
    }

    template <typename AllocT>
    basic_string_view(std::basic_string<CharT, TraitsT, AllocT> const & str) throw()
        : m_begin(str.data()), m_end(m_begin + str.size())
    { }

    basic_string_view(CharT const * first, CharT const * last) throw()
        : m_begin(first), m_end(last)
    { }
    basic_string_view(CharT const * str, size_type len) throw()
        : m_begin(str), m_end(m_begin + len)
    { }
    basic_string_view(CharT const * & str)
        : m_begin(str), m_end(m_begin + TraitsT::length(str))
    { }
    basic_string_view(CharT * str)
        : m_begin(str), m_end(m_begin + TraitsT::length(str))
    { }

    template <size_type L>
    basic_string_view(CharT const (&str)[L]) throw()
        : m_begin(str), m_end(m_begin + ((str[L - 1] == '\0') ? L - 1 : L))
    { }

    const_iterator begin() const throw()
    {
        return m_begin;
    }
    const_iterator end() const throw()
    {
        return m_end;
    }
    const_iterator cbegin() const throw()
    {
        return m_begin;
    }
    const_iterator cend() const throw()
    {
        return m_end;
    }

    const_reverse_iterator rbegin() const throw()
    {
        return const_reverse_iterator(end());
    }
    const_reverse_iterator rend() const throw()
    {
        return const_reverse_iterator(begin());
    }
    const_reverse_iterator crbegin() const throw()
    {
        return const_reverse_iterator(end());
    }
    const_reverse_iterator crend() const throw()
    {
        return const_reverse_iterator(begin());
    }

    size_type size() const throw()
    {
        return std::distance(m_begin, m_end);
    }
    size_type length() const throw()
    {
        return size();
    }
    size_type max_size() const throw()
    {
        return size_type(-1) / sizeof(CharT);
    }
    bool empty() const throw()
    {
        return m_begin == m_end;
    }

    CharT const & operator[](size_type pos) const
    {
        return m_begin[pos];
    }

    CharT const & at(size_type pos) const
    {
        if(pos >= size())
        {
            throw std::out_of_range("string_view::at");
        }
        return m_begin[pos];
    }
    CharT const & front() const
    {
        return *(m_begin + 0);
    }
    CharT const & back() const
    {
        return *(m_end - 1);
    }

    CharT const * data() const throw()
    {
        return m_begin;
    }

    void clear() throw()
    {
        *this = basic_string_view();
    }

    void remove_prefix(size_type n)
    {
        *this = this->substr(n, npos);
    }
    void remove_suffix(size_type n)
    {
        *this = this->substr(0, size() - n);
    }

    void swap(basic_string_view & s) throw()
    {
        std::swap(m_begin, s.m_begin);
        std::swap(m_end, s.m_end);
    }

    template <typename AllocT>
    operator std::basic_string<CharT, TraitsT, AllocT>() const
    {
        return std::basic_string<CharT, TraitsT, AllocT>(begin(), end());
    }

    size_type copy(CharT * s, size_type n, size_type pos = 0) const
    {
        size_type const size = this->size();
        if(pos > size)
        {
            throw std::out_of_range("basic_string_view::copy");
        }
        size_type const rlen = test_limit(size, pos, n);
        if(rlen)
        {
            traits_type::copy(s, data() + pos, rlen);
        }
        return rlen;
    }

    basic_string_view substr(size_type pos = 0, size_type n = npos) const
    {
        size_type const size = this->size();
        if(pos > size)
        {
            throw std::out_of_range("basic_string_view::substr");
        }
        return basic_string_view(data() + pos, std::min(n, size - pos));
    }

    int compare(basic_string_view const & s) const throw()
    {
        size_type const size = this->size();
        size_type const osize = s.size();
        size_type const rlen = std::min(size, osize);

        int r = traits_type::compare(data(), s.data(), rlen);
        if(!r)
        {
            r = size - osize;
        }
        return r;
    }

    int compare(size_type pos1, size_type n1, basic_string_view const & s) const
    {
        size_type const size = this->size();
        if(pos1 > size)
        {
            throw std::out_of_range("basic_string_view::compare");
        }
        n1 = test_limit(size, pos1, n1);
        size_type const osize = s.size();
        size_type const rlen = std::min(n1, osize);

        int r = traits_type::compare(data() + pos1, s.data(), rlen);
        if(!r)
        {
            r = n1 - osize;
        }
        return r;
    }

    int compare(size_type pos1, size_type n1,
                basic_string_view const & s, size_type pos2, size_type n2) const
    {
        size_type const size = this->size();
        if(pos1 > size)
        {
            throw std::out_of_range("basic_string_view::compare");
        }
        size_type const rsize = s.size();
        if(pos2 > rsize)
        {
            throw std::out_of_range("basic_string_view::compare");
        }
        n1 = test_limit(size, pos1, n1);
        n2 = test_limit(rsize, pos2, n2);
        size_type const rlen = std::min(n1, n2);

        int r = traits_type::compare(data() + pos1, s.data() + pos2, rlen);
        if(!r)
        {
            r = n1 - n2;
        }
        return r;
    }
    int compare(CharT const * s) const throw()
    {
        size_type const size = this->size();
        size_type const osize = traits_type::length(s);
        size_type const rlen = std::min(size, osize);

        int r = traits_type::compare(data(), s, rlen);
        if(!r)
        {
            r = size - osize;
        }
        return r;
    }
    int compare(size_type pos1, size_type n1, CharT const * s) const
    {
        size_type const size = this->size();
        if(pos1 > size)
        {
            throw std::out_of_range("basic_string_view::compare");
        }
        n1 = test_limit(size, pos1, n1);
        size_type const osize = traits_type::length(s);
        size_type const rlen = std::min(n1, osize);

        int r = traits_type::compare(data() + pos1, s, rlen);
        if(!r)
        {
            r = n1 - osize;
        }
        return r;
    }
    int compare(size_type pos1, size_type n1, CharT const * s, size_type n2) const
    {
        size_type const size = this->size();
        if(pos1 > size)
        {
            throw std::out_of_range("basic_string_view::compare");
        }
        n1 = test_limit(size, pos1, n1);
        size_type const rlen = std::min(n1, n2);

        int r = traits_type::compare(data() + pos1, s, rlen);
        if(!r)
        {
            r = n1 - n2;
        }
        return r;
    }

    size_type find(basic_string_view const & s, size_type pos = 0) const
    {
        return this->find(s.data(), pos, s.size());
    }
    size_type find(CharT const * s, size_type pos = 0) const
    {
        return this->find(s, pos, traits_type::length(s));
    }
    size_type find(CharT c, size_type pos = 0) const
    {
        size_type ret = npos;
        size_type const size = this->size();
        if(pos < size)
        {
            size_type const n = size - pos;
            CharT const * p = traits_type::find(data() + pos, n, c);
            if(p)
            {
                ret = p - data();
            }
        }
        return ret;
    }
    size_type find(CharT const * s, size_type pos, size_type n) const
    {
        size_type const size = this->size();
        if(n == 0)
        {
            return pos <= size ? pos : npos;
        }
        if(n <= size)
        {
            for(; pos <= size - n; ++pos)
            {
                if(traits_type::eq(*(data() + pos), s[0])
                        && traits_type::compare(data() + pos + 1
                                                , s + 1, n - 1) == 0)
                {
                    return pos;
                }
            }
        }
        return npos;
    }

    size_type rfind(basic_string_view const & s, size_type pos = npos) const
    {
        return this->rfind(s.data(), pos, s.size());
    }
    size_type rfind(CharT const * s, size_type pos = npos) const
    {
        return this->rfind(s, pos, traits_type::length(s));
    }
    size_type rfind(CharT c, size_type pos = npos) const
    {
        size_type size = this->size();
        if(size != 0)
        {
            if(--size > pos)
            {
                size = pos;
            }
            for(++size; size-- > 0; )
            {
                if(traits_type::eq(*(data() + size), c))
                {
                    return size;
                }
            }
        }
        return npos;
    }
    size_type rfind(CharT const * s, size_type pos, size_type n) const
    {
        size_type const size = this->size();
        if(n <= size)
        {
            pos = std::min(size_type(size - n), pos);
            do
            {
                if(traits_type::compare(data() + pos, s, n) == 0)
                {
                    return pos;
                }
            }
            while(pos-- > 0);
        }
        return npos;
    }

    size_type find_first_of(basic_string_view const & s, size_type pos = 0) const
    {
        return this->find_first_of(s.data(), pos, s.size());
    }
    size_type find_last_of(basic_string_view const & s, size_type pos = npos) const
    {
        return this->find_last_of(s.data(), pos, s.size());
    }

    size_type find_first_of(CharT c, size_type pos = 0) const
    {
        return this->find(c, pos);
    }
    size_type find_last_of(CharT c, size_type pos = npos) const
    {
        return this->rfind(c, pos);
    }

    size_type find_first_of(CharT const * s, size_type pos, size_type n) const
    {
        return this->find_first_of(s, pos, traits_type::length(s));
    }
    size_type find_last_of(CharT const * s, size_type pos, size_type n) const
    {
        size_type size = this->size();
        if(size != 0 && n != 0)
        {
            if(--size > pos)
            {
                size = pos;
            }
            do
            {
                if(traits_type::find(s, n, *(data() + size)))
                {
                    return size;
                }
            }
            while(size-- != 0);
        }
        return npos;
    }

    size_type find_first_of(CharT const * s, size_type pos = 0) const
    {
        return this->find_first_of(s, pos, traits_type::length(s));
    }
    size_type find_last_of(CharT const * s, size_type pos = npos) const
    {
        return this->find_last_of(s, pos, traits_type::length(s));
    }

    size_type find_first_not_of(CharT c, size_type pos = 0) const
    {
        for(size_type const size = this->size(); pos < size; ++pos)
        {
            if(!traits_type::eq(*(data() + pos), c))
            {
                return pos;
            }
        }
        return npos;
    }
    size_type find_last_not_of(CharT c, size_type pos = npos) const
    {
        size_type size = this->size();
        if(size != 0)
        {
            if(--size > pos)
            {
                size = pos;
            }
            do
            {
                if(!traits_type::eq(*(data() + size), c))
                {
                    return size;
                }
            }
            while(size-- != 0);
        }
        return npos;
    }

    size_type find_first_not_of(basic_string_view const & s, size_type pos = 0) const
    {
        return this->find_first_not_of(s.data(), pos, s.size());
    }
    size_type find_last_not_of(basic_string_view const & s, size_type pos = npos) const
    {
        return this->find_last_not_of(s.data(), pos, s.size());
    }

    size_type find_first_not_of(CharT const * s, size_type pos, size_type n) const
    {
        for(size_type const size = this->size(); pos < size; ++pos)
        {
            if(!traits_type::find(s, n, *(data() + pos)))
            {
                return pos;
            }
        }
        return npos;
    }
    size_type find_last_not_of(CharT const * s, size_type pos, size_type n) const
    {
        size_type size = this->size();
        if(size != 0)
        {
            if(--size > pos)
            {
                size = pos;
            }
            do
            {
                if(!traits_type::find(s, n, *(data() + size)))
                {
                    return size;
                }
            }
            while(size-- != 0);
        }
        return npos;
    }

    size_type find_first_not_of(CharT const * s, size_type pos = 0) const
    {
        return this->find_first_not_of(s, pos, traits_type::length(s));
    }
    size_type find_last_not_of(CharT const * s, size_type pos = npos) const
    {
        return this->find_last_not_of(s, pos, traits_type::length(s));
    }

private:
    static size_type test_limit(size_type size, size_type pos, size_type off) throw()
    {
        return (off < size - pos) ? off : size - pos;
    }

    const_iterator m_begin, m_end;
};

// basic_string_view typedef names
typedef basic_string_view<char>    string_view;
typedef basic_string_view<wchar_t> wstring_view;

} // namespace utility

#endif // UTILITY_STRING_VIEW_H_INCLUDED_

