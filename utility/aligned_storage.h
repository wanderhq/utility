#ifndef UTILITY_ALIGNED_STORAGE_H_INCLUDED_
#define UTILITY_ALIGNED_STORAGE_H_INCLUDED_

#include <cstddef>
#include "max_align_type.h"

namespace utility
{

template <typename T, size_t Len>
union aligned_union
{
    T    aligner;
    char dummy[Len];
};

template <size_t Len, size_t Align, typename T, bool Ok>
struct aligned_next;

template <size_t Len, size_t Align, typename T>
struct aligned_next<Len, Align, T, true>
{
    typedef aligned_union<T, Len> type;
};

//End of search defaults to max_align_t
template <size_t Len, size_t Align>
struct aligned_next<Len, Align, max_align_t, false>
{
    typedef aligned_union<max_align_t, Len> type;
};

//Now define a search list through types
#define UTILITY_MOVE_ALIGNED_NEXT_STEP(TYPE, NEXT_TYPE)\
   template<size_t Len, size_t Align>\
   struct aligned_next<Len, Align, TYPE, false>\
      : aligned_next<Len, Align, NEXT_TYPE, (Align == ct::align_of<NEXT_TYPE>::value)>\
   { };\
   //
   UTILITY_MOVE_ALIGNED_NEXT_STEP(long double, max_align_t)
   UTILITY_MOVE_ALIGNED_NEXT_STEP(double, long double)
   UTILITY_MOVE_ALIGNED_NEXT_STEP(long long, double)
   UTILITY_MOVE_ALIGNED_NEXT_STEP(long, long long)
   UTILITY_MOVE_ALIGNED_NEXT_STEP(int, long)
   UTILITY_MOVE_ALIGNED_NEXT_STEP(short, int)
   UTILITY_MOVE_ALIGNED_NEXT_STEP(char, short)
   //
#undef UTILITY_MOVE_ALIGNED_NEXT_STEP

template <size_t Len, size_t Align>
struct aligned_storage_impl
   : aligned_next<Len, Align, char, (Align == ct::align_of<char>::value)>
{ };

template <size_t Len, size_t Align = ct::align_of<max_align_t>::value>
struct aligned_storage
    // Sanity checks for output type
    : aligned_storage_impl<(Len ? Len : 1), Align>
{
    static const size_t value = ct::align_of<
        typename aligned_storage::type
    >::value;

private:
    aligned_storage();
};

} // namespace utility

#endif // UTILITY_ALIGNED_STORAGE_H_INCLUDED_
