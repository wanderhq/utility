#ifndef UTILITY_LOW_LEVEL_FIELD_H_INCLUDED_
#define UTILITY_LOW_LEVEL_FIELD_H_INCLUDED_

#include <cstddef>
#include <cassert>
#include <algorithm>
#include <stdint.h>
#include <cstdio>

#include "../ct/type_traits.h"
#include "../ct/align_of.h"

namespace utility
{

template <typename ValueType, typename Storage, typename Prev>
struct low_level_field_base
{
private:
    Storage * storage()
    {
        return static_cast<Storage *>(this);
    }
    Storage const * storage() const
    {
        return static_cast<Storage const *>(this);
    }

    typedef typename ct::remove_all_extents<ValueType>::type element_type;
    enum
    {
        block_size = sizeof(element_type)
    };

protected:
    /* For dynamic memory management*/
    size_t storage_reloc_memory(void const * src, size_t size)
    {
        size_t const old_size = storage()->size_of();
        storage()->reloc_memory(offset(), old_size, src, size);
        return size;
    }
    size_t storage_reloc_nbo_memory(void const * src, size_t size)
    {
        size_t const old_size = storage()->size_of();
        storage()->template
            reloc_nbo_memory<block_size>(offset(), old_size, src, size);
        return size;
    }

    size_t storage_reloc_memory(size_t size)
    {
        size_t const old_size = storage()->size_of();
        storage()->reloc_memory(offset(), old_size, size);
        return size;
    }

    /*For static memory management */
    void storage_set_memory(void const * src, size_t size)
    {
        size_t const old_size = storage()->size_of();
        storage()->set_memory(offset(), old_size, src, size);
    }
    void storage_set_nbo_memory(void const * src, size_t size)
    {
        size_t const old_size = storage()->size_of();
        storage()->template
            set_nbo_memory<block_size>(offset(), old_size, src, size);
    }

    /*==*/
    void storage_set_memory(size_t off, void const * src, size_t size)
    {
        assert(off < storage()->size_of());

        storage()->set_memory(offset() + off, size, src, size);
    }
    void storage_set_nbo_memory(size_t off, void const * src, size_t size)
    {
        assert(off < storage()->size_of());

        storage()->template
            set_nbo_memory<block_size>(offset() + off, size, src, size);
    }
    /*==*/
    size_t storage_reloc_memory(size_t off, void const * src, size_t size)
    {
        assert(off <= storage()->size_of());

        size_t old_size = storage()->size_of() - off;
        if(old_size < size)
        {
            storage()->reloc_memory(offset() + off, old_size, size);
            old_size = size;
        }
        storage()->set_memory(offset() + off, size, src, size);
        return old_size + off;
    }
    size_t storage_reloc_nbo_memory(size_t off, void const * src, size_t size)
    {
        assert(off <= storage()->size_of());

        size_t old_size = storage()->size_of() - off;
        if(old_size < size)
        {
            storage()->reloc_memory(offset() + off, old_size, src, size);
            old_size = size;
        }
        storage()->template
            set_nbo_memory<block_size>(offset() + off, size, src, size);
        return old_size + off;
    }

protected:
    size_t offset_end() const
    {
        return offset() + storage()->size_of();
    }

public:
    typedef ValueType value_type;

    enum
    {
        value_size = sizeof(value_type)
    };

    void * raw_memory()
    {
        return storage()->raw_memory_by(offset());
    }
    void const * raw_memory() const
    {
        return storage()->raw_memory_by(offset());
    }

    value_type const * get() const
    {
        return static_cast<value_type const *>(raw_memory());
    }

    size_t offset() const
    {
        return storage()->Prev::offset_end();
    }

    bool is_aligned() const
    {
        enum
        {
            AlignOfValueType = ct::align_of<ValueType>::value
        };
        return !(reinterpret_cast<intptr_t>(get()) % AlignOfValueType);
    }

    void copy(value_type & val) const
    {
        assert(value_size == storage()->size_of());

        storage()->copy_memory(offset(), &val, value_size);
    }
    void copy_network(value_type & val) const
    {
        assert(value_size == storage()->size_of());

        storage()->template
            copy_nbo_memory<block_size>(offset(), &val, value_size);
    }

    void copy(value_type & val, size_t idx) const
    {
        size_t const off = idx * value_size;

        assert(off < storage()->size_of());

        storage()->copy_memory(offset() + off, &val, value_size);
    }
    void copy_network(value_type & val, size_t idx) const
    {
        size_t const off = idx * value_size;

        assert(off < storage()->size_of());

        storage()->template
            copy_nbo_memory<block_size>(offset() + off, &val, value_size);
    }

    template <size_t N>
    void copy(value_type (& val)[N]) const
    {
        assert(sizeof(val) <= storage()->size_of());

        storage()->copy_memory(offset(), val, sizeof(val));
    }
    template <size_t N>
    void copy_network(value_type (& val)[N]) const
    {
        assert(sizeof(val) <= storage()->size_of());

        storage()->template
            copy_nbo_memory<block_size>(offset(), val, sizeof(val));
    }

    void zero_memory(size_t const off)
    {
        assert(off <= storage()->size_of());

        size_t const size = storage()->size_of() - off;
        storage()->zero_memory_by(offset() + off, size);
    }
    void fill_memory(size_t const off, unsigned char byte, size_t size)
    {
        assert(off <= storage()->size_of());
        assert(size <= (storage()->size_of() - off));

        storage()->fill_memory_by(offset() + off, byte, size);
    }
    void copy_memory(size_t const off, void const * src, size_t size)
    {
        assert(off <= storage()->size_of());
        assert(size <= (storage()->size_of() - off));

        storage()->copy_memory_by(offset() + off, src, size);
    }
    void move_memory(size_t const off, void const * src, size_t size)
    {
        assert(off <= storage()->size_of());
        assert(size <= (storage()->size_of() - off));

        storage()->move_memory_by(offset() + off, src, size);
    }
    int compare_memory(size_t const off, void const * src, size_t size) const
    {
        assert(off <= storage()->size_of());

        size_t const old_size = storage()->size_of() - off;
        size_t const opt_size = std::min(old_size, size);

        int r = storage()->compare_memory_by(offset() + off, src, opt_size);
        if(!r)
        {
            r = old_size - size;
        }
        return r;
    }
};

/*! For static sized types
 */
template <typename T, typename Storage, typename Prev>
struct low_level_field
    : low_level_field_base<
          typename ct::remove_extent<T>::type
        , Storage
        , Prev
      >
{
    typedef T                                     field_type;
    typedef typename ct::remove_extent<T>::type   value_type;

    static bool   const is_dynamic   = false;
    static size_t const static_size  = sizeof(field_type);
    static size_t const static_count = static_size / sizeof(value_type);

    void set(value_type const & val)
    {
        this->storage_set_memory(&val, sizeof(val));
    }
    void set(value_type const * p, size_t count)
    {
        this->storage_set_memory(p, count * sizeof(*p));
    }
    template <size_t N>
    void set(value_type const (& val)[N])
    {
        this->storage_set_memory(val, sizeof(val));
    }

    void set_network(value_type const & val)
    {
        this->storage_set_nbo_memory(&val, sizeof(val));
    }
    void set_network(value_type const * p, size_t count)
    {
        this->storage_set_nbo_memory(p, count * sizeof(*p));
    }
    template <size_t N>
    void set_network(value_type const (& val)[N])
    {
        this->storage_set_nbo_memory(val, sizeof(val));
    }

    void insert(size_t const idx, value_type const & val)
    {
        size_t const off = idx * sizeof(val);
        this->storage_set_memory(off, &val, sizeof(val));
    }
    void insert(size_t const idx, value_type const * p, size_t count)
    {
        size_t const off = idx * sizeof(*p);
        this->storage_set_memory(off, p, count * sizeof(*p));
    }
    template <size_t N>
    void insert(size_t const idx, value_type const (& val)[N])
    {
        size_t const off = idx * sizeof(val[0]);
        this->storage_set_memory(off, val, sizeof(val));
    }

    void insert_network(size_t const idx, value_type const & val)
    {
        size_t const off = idx * sizeof(val);
        this->storage_set_nbo_memory(off, &val, sizeof(val));
    }
    void insert_network(size_t const idx, value_type const * p, size_t count)
    {
        size_t const off = idx * sizeof(*p);
        this->storage_set_nbo_memory(off, p, count * sizeof(*p));
    }
    template <size_t N>
    void insert_network(size_t const idx, value_type const (& val)[N])
    {
        size_t const off = idx * sizeof(val[0]);
        this->storage_set_nbo_memory(off, val, sizeof(val));
    }

    void set_count(size_t)
    { }

    // in elements
    size_t count() const
    {
        return static_count;
    }

    field_type const & restore() const
    {
        return *static_cast<field_type const *>(this->raw_memory());
    }

    size_t size_of() const
    {
        return static_size;
    }
};

/*! For dynamic sized types
 */
template <typename T, typename Storage, typename Prev>
struct low_level_field<T[], Storage, Prev>
    : low_level_field_base<T, Storage, Prev>
{
    typedef T   value_type;
    typedef T   field_type  [];

    static bool   const is_dynamic   = true;
    static size_t const static_size  = 0;
    static size_t const static_count = 0;

    low_level_field()
        : m_size()
    { }

    void set(value_type const & val)
    {
        m_size = this->storage_reloc_memory(&val, sizeof(val));
    }
    void set(value_type const * p, size_t count)
    {
        m_size = this->storage_reloc_memory(p, count * sizeof(*p));
    }
    template <size_t N>
    void set(value_type const (& val)[N])
    {
        m_size = this->storage_reloc_memory(val, sizeof(val));
    }

    void set_network(value_type const & val)
    {
        m_size = this->storage_reloc_nbo_memory(&val, sizeof(val));
    }
    void set_network(value_type const * p, size_t count)
    {
        m_size = this->storage_reloc_nbo_memory(p, count * sizeof(*p));
    }
    template <size_t N>
    void set_network(value_type const (& val)[N])
    {
        m_size = this->storage_reloc_nbo_memory(val, sizeof(val));
    }

    void insert(size_t const idx, value_type const & val)
    {
        size_t const off = idx * sizeof(val);
        m_size = this->storage_reloc_memory(off, &val, sizeof(val));
    }
    void insert(size_t const idx, value_type const * p, size_t count)
    {
        size_t const off = idx * sizeof(*p);
        m_size = this->storage_reloc_memory(off, p, count * sizeof(*p));
    }
    template <size_t N>
    void insert(size_t const idx, value_type const (& val)[N])
    {
        size_t const off = idx * sizeof(val[0]);
        m_size = this->storage_reloc_memory(off, val, sizeof(val));
    }

    void insert_network(size_t const idx, value_type const & val)
    {
        size_t const off = idx * sizeof(val);
        m_size = this->storage_reloc_nbo_memory(off, &val, sizeof(val));
    }
    void insert_network(size_t const idx, value_type const * p, size_t count)
    {
        size_t const off = idx * sizeof(*p);
        m_size = this->storage_reloc_nbo_memory(off, p, count * sizeof(*p));
    }
    template <size_t N>
    void insert_network(size_t const idx, value_type const (& val)[N])
    {
        size_t const off = idx * sizeof(val[0]);
        m_size = this->storage_reloc_nbo_memory(off, val, sizeof(val));
    }

    void set_count(size_t count)
    {
        m_size = this->storage_reloc_memory(count * sizeof(value_type));
    }

    // in elements
    size_t count() const
    {
        return m_size / sizeof(value_type);
    }

    field_type const & restore() const
    {
        return *static_cast<field_type const *>(this->raw_memory());
    }

    size_t size_of() const
    {
        return m_size;
    }

private:
    size_t m_size;
};

} // namespace utility

#endif // UTILITY_LOW_LEVEL_FIELD_H_INCLUDED_
