#ifndef UTILITY_LOW_LEVEL_STORAGE_H_INCLUDED_
#define UTILITY_LOW_LEVEL_STORAGE_H_INCLUDED_

#include "low_level_field.h"

#include "../ct/type_list.h"
#include "../ct/type_traits.h"
#include "../ct/algorithm.h"

#include "../endian_reverse.h"
#include "../aligned_type.h"

#include <cstring>
#include <cassert>
#include <cstdlib>
#include <stdexcept>
#include <cstdio>

namespace utility
{

namespace detail
{

template <typename T>
struct size_of_helper
    : ct::integral_constant<size_t, sizeof(T)>
{ };
template <typename T>
struct size_of_helper<T[]>
    : ct::integral_constant<size_t, 0>
{ };

} // namespace detail

template <typename ListT, bool Static = true, size_t Size = 0, size_t MaxAlign = 1>
struct low_level_storage;

template <typename T, typename Self, typename Prev>
struct low_level_storage_composer
    : low_level_field<T, Self, Prev>, Prev
{
    typedef low_level_field<T, Self, Prev> field_type;

    using field_type::offset_end;
    using field_type::offset;
    using field_type::size_of;

    typedef low_level_storage_composer composer;

    explicit low_level_storage_composer(size_t capacity)
        : Prev(capacity)
    { }
    low_level_storage_composer()
        : Prev()
    { }

    using Prev::zero_memory;
    using Prev::raw_memory;
    using Prev::copy_memory;
    using Prev::size;
};

#if __cplusplus >= 201103L // C++11
template <typename T, typename ...Tail, bool Static, size_t Size, size_t MaxAlign>
struct low_level_storage<ct::type_list<T, Tail...>, Static, Size, MaxAlign>
    : low_level_storage_composer<
         T
       , low_level_storage<
             ct::type_list<T, Tail...>
           , Static
           , Size
           , MaxAlign
         >
       , low_level_storage<
             ct::type_list<Tail...>
           , Static * !ct::is_array_of_unknown_bound<T>::value
           , Size + detail::size_of_helper<T>::value
           , ct::max<MaxAlign, ct::align_of<T>::value>::value
         >
      >
{
    using low_level_storage::composer::composer;
};
#else
template <typename T, typename Tail, bool Static, size_t Size, size_t MaxAlign>
struct low_level_storage<ct::type_list<T, Tail>, Static, Size, MaxAlign>
    : low_level_storage_composer<
         T
       , low_level_storage<
             ct::type_list<T, Tail>
           , Static
           , Size
           , MaxAlign
         >
       , low_level_storage<
             Tail
           , Static * !ct::is_array_of_unknown_bound<T>::value
           , Size + detail::size_of_helper<T>::value
           , ct::max<MaxAlign, ct::align_of<T>::value>::value
         >
      >
{
    explicit low_level_storage(size_t capacity)
        : low_level_storage::composer(capacity)
    { }
    low_level_storage()
        : low_level_storage::composer()
    { }
};
#endif

template <typename Storage>
struct low_level_storage_base
{
private:
    char * memory()
    {
        return static_cast<Storage *>(this)->bytes();
    }
    char const * memory() const
    {
        return static_cast<Storage const *>(this)->bytes();
    }
    size_t memory_size() const
    {
        return static_cast<Storage const *>(this)->size();
    }

    char * memory(size_t offset)
    {
        return memory() + offset;
    }
    char const * memory(size_t offset) const
    {
        return memory() + offset;
    }

public:
    size_t offset() const
    {
        return 0;
    }
    size_t offset_end() const
    {
        return 0;
    }

    void set_memory(size_t offset, size_t total, void const * src, size_t size)
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());
        assert(total >= size);

        std::memcpy(memory(offset), src, size);
        std::memset(memory(offset + size), 0, total - size);
    }
    template <size_t BlockSize>
    void set_nbo_memory(size_t offset, size_t total, void const * src, size_t size)
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());
        assert(total >= size);

        endian_reverse<BlockSize>(memory(offset), src, size);
        std::memset(memory(offset + size), 0, total - size);
    }

    void zero_memory()
    {
        assert(memory() != 0);

        std::memset(memory(), 0, memory_size());
    }

    void copy_memory(size_t offset, void * dest, size_t size) const
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());

        std::memcpy(dest, memory(offset), size);
    }
    template <size_t BlockSize>
    void copy_nbo_memory(size_t offset, void * dest, size_t size) const
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());

        endian_reverse<BlockSize>(dest, memory(offset), size);
    }

    bool equal(low_level_storage_base const & x) const
    {
        return x.memory_size() == memory_size()
            && std::memcmp(x.memory(), memory(), x.memory_size()) == 0;
    }

    void * raw_memory_by(size_t offset)
    {
        assert(memory() != 0);
        assert(offset <= memory_size());

        return memory(offset);
    }
    void const * raw_memory_by(size_t offset) const
    {
        assert(memory() != 0);
        assert(offset <= memory_size());

        return memory(offset);
    }

    void fill_memory_by(size_t offset, unsigned char const byte, size_t size)
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());

        std::memset(memory(offset), byte, size);
    }
    void zero_memory_by(size_t offset, size_t size)
    {
        fill_memory_by(offset, 0, size);
    }

    void move_memory_by(size_t offset, void const * src, size_t size)
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());

        std::memmove(memory(offset), src, size);
    }
    void copy_memory_by(size_t offset, void const * src, size_t size)
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());

        std::memcpy(memory(offset), src, size);
    }
    int compare_memory_by(size_t offset, void const * mem, size_t size) const
    {
        assert(memory() != 0);
        assert(offset + size <= memory_size());

        return std::memcmp(memory(offset), mem, size);
    }
};

template <size_t MaxAlign>
struct low_level_storage<ct::null_list, true, 0, MaxAlign>
    : low_level_storage_base<
          low_level_storage<ct::null_list, true, 0, MaxAlign>
      >
{
    low_level_storage(size_t capacity)
    {
        (void)capacity;
    }
    low_level_storage()
    {
    }

    void swap(low_level_storage & other)
    {
        (void)other;
    }

    size_t size() const
    {
        return 0;
    }
    size_t capacity() const
    {
        return 0;
    }
    char const * bytes() const
    {
        return 0;
    }
    char * bytes()
    {
        return 0;
    }
    void * raw_memory()
    {
        return 0;
    }
    void const * raw_memory() const
    {
        return 0;
    }
    void zero_memory()
    {
    }
    void reserve(size_t capacity)
    {
        (void)capacity;
    }
};

template <size_t Size, size_t MaxAlign>
struct low_level_storage<ct::null_list, true, Size, MaxAlign>
    : low_level_storage_base<
          low_level_storage<ct::null_list, true, Size, MaxAlign>
      >
{
public:
    enum
    {
        static_capacity = Size
    };

    low_level_storage(size_t)
    { }
    low_level_storage()
    { }

    void swap(low_level_storage & other)
    {
        char tmp_memory[Size];
        std::memcpy(tmp_memory, m_memory, Size);
        std::memcpy(m_memory, other.m_memory, Size);
        std::memcpy(other.m_memory, tmp_memory, Size);
    }

// public interface
    void * raw_memory()
    {
        return &m_memory;
    }
    void const * raw_memory() const
    {
        return &m_memory;
    }
    size_t size() const
    {
        return Size;
    }
    size_t capacity() const
    {
        return Size;
    }
    char const * bytes() const
    {
        return static_cast<char const *>(raw_memory());
    }
    char * bytes()
    {
        return static_cast<char *>(raw_memory());
    }
    void reserve(size_t capacity)
    {
        (void)capacity;
    }
// -- end public interface

private:
    typename make_aligned<char [Size], MaxAlign>::type m_memory;
};

template <size_t Size, size_t MaxAlign>
struct low_level_storage<ct::null_list, false, Size, MaxAlign>
    : low_level_storage_base<
          low_level_storage<ct::null_list, false, Size, MaxAlign>
      >
{
private:
    static char * checked_malloc(size_t capacity)
    {
        void * dest = std::malloc(capacity);
        if(!dest)
        {
            throw std::bad_alloc();
        }
        return static_cast<char *>(dest);
    }
    static void checked_free(void * ptr)
    {
        std::free(ptr);
    }
    static char * alloc_copy(char const * src, size_t capacity, size_t size)
    {
        char * dest = 0;
        if(capacity)
        {
            dest = checked_malloc(capacity);
            if(size)
            {
                std::memcpy(dest, src, size);
            }
        }
        return dest;
    }

public:
    enum
    {
        static_capacity = Size
    };

    explicit low_level_storage(size_t capacity)
        : m_capacity(std::max(capacity, Size))
        , m_size(Size)
        , m_memory(m_capacity ? checked_malloc(m_capacity) : 0)
    { }
    low_level_storage()
        : m_capacity(Size)
        , m_size(Size)
        , m_memory(checked_malloc(Size))
    { }
    low_level_storage(low_level_storage const & x)
        : m_capacity(x.m_size)
        , m_size(x.m_size)
        , m_memory(alloc_copy(x.m_memory, m_size, m_size))
    { }
    low_level_storage & operator=(low_level_storage const & x)
    {
        if(this != &x)
        {
            low_level_storage(x).swap(*this);
        }
        return *this;
    }
    ~low_level_storage()
    {
        checked_free(m_memory);
    }

    void swap(low_level_storage & other)
    {
        std::swap(other.m_size, m_size);
        std::swap(other.m_memory, m_memory);
        std::swap(other.m_capacity, m_capacity);
    }

// public interface
    void * raw_memory()
    {
        return m_memory;
    }
    void const * raw_memory() const
    {
        return m_memory;
    }
    size_t size() const
    {
        return m_size;
    }
    char const * bytes() const
    {
        return m_memory;
    }
    char * bytes()
    {
        return m_memory;
    }

    size_t capacity() const
    {
        return m_capacity;
    }
    void reserve(size_t capacity)
    {
        if(capacity > m_capacity)
        {
            char * mem = alloc_copy(capacity, m_size);
            checked_free(m_memory);
            m_memory = mem;
        }
    }
// -- end public interface

    void reloc_memory(size_t offset, size_t old, void const * src, size_t size)
    {
        realloc(offset, old, size);
        this->set_memory(offset, size, src, size);
    }
    template <size_t BlockSize>
    void reloc_nbo_memory(size_t offset, size_t old, void const * src, size_t size)
    {
        realloc(offset, old, size);
        this->template set_nbo_memory<BlockSize>(offset, size, src, size);
    }
    void reloc_memory(size_t offset, size_t old, size_t size)
    {
        realloc(offset, old, size);
    }

private:
    char * alloc_copy(size_t capacity, size_t size)
    {
        char * mem = alloc_copy(m_memory, capacity, size);
        m_capacity = capacity;
        return mem;
    }

    void realloc(size_t offset, size_t old_size, size_t new_size)
    {
        if(old_size != new_size)
        {
            size_t const suffix_offset = offset + old_size;
            size_t const suffix_new_offset = offset + new_size;
            size_t const suffix_size = m_size - suffix_offset;

            size_t const capacity = (m_size - old_size) + new_size;
            if(capacity > m_capacity)
            {
                char * mem = alloc_copy(capacity, offset);

                char * suffix_dest = mem + suffix_new_offset;
                std::memcpy(suffix_dest, m_memory + suffix_offset, suffix_size);

                checked_free(m_memory);
                m_memory = mem;
            }
            else if(suffix_size != 0)
            {
                char * suffix_dest = m_memory + suffix_new_offset;
                std::memmove(suffix_dest, m_memory + suffix_offset, suffix_size);
            }
            m_size = capacity;
        }
    }

private:
    size_t m_capacity;
    size_t m_size;
    char * m_memory;
};

template <>
inline
low_level_storage<ct::null_list, false, 0>::low_level_storage()
    : m_capacity()
    , m_size()
    , m_memory()
{ }

template <>
inline
low_level_storage<ct::null_list, false, 0>::low_level_storage(size_t capacity)
    : m_capacity(capacity)
    , m_size()
    , m_memory(m_capacity ? checked_malloc(m_capacity) : 0)
{ }

} // namespace utility

#endif // UTILITY_LOW_LEVEL_STORAGE_H_INCLUDED_
