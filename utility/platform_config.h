#ifndef UTILITY_PLATFORM_CONFIG_H_INCLUDED_
#define UTILITY_PLATFORM_CONFIG_H_INCLUDED_

#if defined(__CYGWIN__) // CYGWIN in not Win32
#   define PLATFORM_CYGWIN 1
#   if !defined(PLATFORM_NO_POSIX_COMPL_REQUIRED)
#       define PLATFORM_POSIX  1

#   endif

#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#   define PLATFORM_WIN32      1

#elif defined(linux) || defined(__linux) || defined(__linux__)
#   define PLATFORM_LINUX  1
#   if !defined(PLATFORM_NO_POSIX_COMPL_REQUIRED)
#       define PLATFORM_POSIX  1

#   endif

#elif defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__DragonFly__)
#   define PLATFORM_BSD    1
#   if !defined(PLATFORM_NO_POSIX_COMPL_REQUIRED)
#       define PLATFORM_POSIX  1

#   endif

#else
#   warning "Unknown platform"
#endif

#if defined __DMC__
#   define COMPILER_DMC __DMC__

#elif defined(__INTEL_COMPILER) || defined(__ICL) || defined(__ICC) || defined(__ECC)
#   define COMPILER_INTEL __INTEL_COMPILER

#elif defined(__BORLANDC__)
#   define COMPILER_BORLAND __BORLANDC__

#elif defined(_MSC_VER)
#   define COMPILER_MSVC _MSC_FULL_VER

#elif defined(__MINGW32__)
#   define COMPILER_MINGW (__GNUC__ * 100 + __GNUC_MINOR__)

#elif defined(__clang__)
#   define COMPILER_CLANG (__clang_major__ * 100 + __clang_minor__)

#elif defined(__GNUC__)
#   define COMPILER_GCC (__GNUC__ * 100 + __GNUC_MINOR__)

#else
#   warning "Unknown compiler"
#endif

#endif // UTILITY_PLATFORM_CONFIG_H_INCLUDED_
