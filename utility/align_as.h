#ifndef UTILITY_ALIGN_AS_H_INCLUDED_
#define UTILITY_ALIGN_AS_H_INCLUDED_

#include "platform_config.h"

#if __cplusplus >= 201103L // C++11
#   include <cstdarg>
#endif

#if   defined(__alignas_is_defined)
#   define UTILITY_ALIGN_AS_SUPPORTED
#   define UTILITY_ALIGN_AS(Align) alignas(Align)

#elif defined(COMPILER_MSVC)  || defined(COMPILER_DMC) || defined(COMPILER_INTEL)
#   define UTILITY_ALIGN_AS_SUPPORTED
#   define UTILITY_ALIGN_AS(Align) __declspec (align(Align))

#elif defined(COMPILER_MINGW) || defined(COMPILER_GCC) || defined(COMPILER_CLANG)
#   define UTILITY_ALIGN_AS_SUPPORTED
#   define UTILITY_ALIGN_AS(Align) __attribute__ ((aligned(Align)))

#endif

#endif // UTILITY_ALIGN_AS_H_INCLUDED_
