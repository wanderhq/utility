#ifndef UTILITY_SAFE_BOOL_H_INCLUDED_
#define UTILITY_SAFE_BOOL_H_INCLUDED_

namespace utility
{

class safe_bool_base
{
public:
    typedef void (safe_bool_base::*bool_type)() const;

    void this_type_does_not_support_comparisons() const
    { }

protected:
    safe_bool_base()
    { }
    safe_bool_base(safe_bool_base const &)
    { }
    safe_bool_base & operator=(safe_bool_base const &)
    {
        return *this;
    }
    ~safe_bool_base()
    { }
};

template <typename T>
struct safe_bool
    : private safe_bool_base
{
    operator safe_bool_base::bool_type() const
    {
        return static_cast<T const *>(this)->operator_bool()
               ? &safe_bool_base::this_type_does_not_support_comparisons
               : 0;
    }

protected:
    ~safe_bool()
    { }
};

template <typename T>
bool operator==(safe_bool<T> const & lhs, bool b)
{
    return b == static_cast<bool>(lhs);
}

template <typename T>
bool operator==(bool b, safe_bool<T> const & rhs)
{
    return b == static_cast<bool>(rhs);
}

template <typename T, typename U>
bool operator==(safe_bool<T> const & lhs, safe_bool<U> const & rhs)
{
    lhs.this_type_does_not_support_comparisons();
    return false;
}

template <typename T,typename U>
bool operator!=(safe_bool<T> const & lhs, safe_bool<U> const & rhs)
{
    lhs.this_type_does_not_support_comparisons();
    return false;
}

} // namespace utility

#endif // UTILITY_SAFE_BOOL_H_INCLUDED_
