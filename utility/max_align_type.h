#ifndef UTILITY_MAX_ALIGN_TYPE_H_INCLUDED_
#define UTILITY_MAX_ALIGN_TYPE_H_INCLUDED_

namespace utility
{
namespace detail
{

struct dummy_base0
{
    virtual void dummy() const = 0;
};

struct dummy_base1
{
    int dummy;
};

struct dummy_derived0
    : virtual dummy_base0
{ };

struct dummy_derived1
    : virtual dummy_base0, dummy_derived0, dummy_base1
{ };

struct dummy_struct
{
    long double dummy[4];
};

} // namespace detail

union max_align_t
{
    char          dummy00;
    short         dummy01;
    int           dummy02;
    long          dummy03;
    long long     dummy04;
    signed char   dummy05;
    bool          dummy06;

    unsigned char      dummy10;
    unsigned short     dummy11;
    unsigned int       dummy12;
    unsigned long      dummy13;
    unsigned long long dummy14;

    float       dummy20;
    double      dummy21;
    long double dummy22;
    long double dummy23[4];

    void (*                         dummy30)();
    void (detail::dummy_derived1::* dummy31)();
    void (detail::dummy_derived0::* dummy32)();
    int  (detail::dummy_base1::   * dummy33);

    void        * dummy40;
    max_align_t * dummy41;

    detail::dummy_struct dummy50;
};

} // namespace utility

#endif // UTILITY_MAX_ALIGN_TYPE_H_INCLUDED_
