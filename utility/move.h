#ifndef UTILITY_MOVE_H_INCLUDED_
#define UTILITY_MOVE_H_INCLUDED_

#include "ct/enable_if.h"
#include "ct/identity.h"
#include "ct/type_traits.h"

namespace utility
{
/*!*/
template <typename T>
class rv
{
public:
    explicit rv(T & r)
        : m_r(r)
    { }

    T * operator->()
    {
        return &m_r;
    }
    T & operator *()
    {
        return m_r;
    }

private:
    T & m_r;
};
/*!*/

template <typename T>
inline
typename ct::enable_if<
    !ct::is_convertible<T, rv<T> >::value, T &
>::type move(T & t)
{
    return t;
}

template <typename T>
inline
typename ct::enable_if<
    !ct::is_convertible<T, rv<T> >::value, T const &
>::type move(T const & t)
{
    return t;
}

template <typename T>
inline
typename ct::enable_if<
    ct::is_convertible<T, rv<T> >::value, T
>::type move(T & t)
{
    return T(rv<T>(t));
}

template <typename T>
inline
typename ct::enable_if<
    ct::is_reference<T>::value, T
>::type forward(typename ct::identity<T>::type t)
{
    return t;
}

template <typename T>
inline
typename ct::enable_if<
    !ct::is_reference<T>::value, T
>::type forward(typename ct::identity<T>::type & t)
{
    return move(t);
}

template <typename T>
inline
typename ct::enable_if<
    !ct::is_reference<T>::value, T
>::type forward(typename ct::identity<T>::type const & t)
{
    return move(const_cast<T &>(t));
}
/*!*/
} // namespace utility

#endif // UTILITY_MOVE_H_INCLUDED_
