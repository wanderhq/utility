#ifndef UTILITY_LIBRARY_HANDLE_H_INCLUDED_
#define UTILITY_LIBRARY_HANDLE_H_INCLUDED_

#include "../noncopyable.h"
#include "../safe_bool.h"

#include "../ct/type_traits.h"
#include "../ct/enable_if.h"

#include <dlfcn.h>
#include <algorithm>

namespace utility
{

class shared_lib
    : noncopyable, safe_bool<shared_lib>
{
public:
    explicit shared_lib(char const * path, int flags = RTLD_LOCAL | RTLD_NOW)
        : m_handle(::dlopen(path, flags))
    { }

    shared_lib()
        : m_handle()
    { }

    ~shared_lib() throw()
    {
        unload();
    }

    bool load(char const * path, int flags = RTLD_LOCAL | RTLD_NOW)
    {
        shared_lib(path, flags).swap(*this);
        return is_valid();
    }

    int unload() throw()
    {
        return is_valid() ? ::dlclose(m_handle) : 0;
    }

    template <typename F>
    typename ct::enable_if<ct::is_function<F>::value, F>::type *
        address_of(char const * name) const
    {
        return reinterpret_cast<F *>(::dlsym(m_handle, name));
    }

    bool is_valid() const
    {
        return m_handle != 0;
    }

    char const * last_error() const
    {
        return ::dlerror();
    }

    void swap(shared_lib & other)
    {
        std::swap(m_handle, other.m_handle);
    }

    bool operator_bool() const
    {
        return is_valid();
    }

private:
    void * m_handle;
};

} // namespace utility

#endif // UTILITY_LIBRARY_HANDLE_H_INCLUDED_
