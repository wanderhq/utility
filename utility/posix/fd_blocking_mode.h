#ifndef UTILITY_FD_BLOCKING_MODE_H_INCLUDED_
#define UTILITY_FD_BLOCKING_MODE_H_INCLUDED_

#include <unistd.h>
#include <fcntl.h>

namespace utility
{

inline
bool set_fd_blocking(int fd, bool blocking)
{
    int flags;
    /* If they have O_NONBLOCK, use the POSIX way to do it */
#if defined(O_NONBLOCK)
    /* FIXME: O_NONBLOCK is defined but broken on SunOS 4.1.x and AIX 3.2.5. */
    if(-1 == (flags = fcntl(fd, F_GETFL, 0)))
    {
        flags = 0;
    }
    flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);

    return fcntl(fd, F_SETFL, flags) != -1;
#else
    /* Otherwise, use the old way of doing it */
    flags = !blocking;
    return ioctl(fd, FIOBIO, &flags) != -1;
#endif
}

inline
bool get_fd_blocking(int fd)
{
    return fcntl(fd, F_GETFL) & ~O_NONBLOCK;
}

} // namespace utility

#endif // UTILITY_FD_BLOCKING_MODE_H_INCLUDED_
