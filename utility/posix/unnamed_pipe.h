#ifndef UTILITY_UNNAMED_PIPE_H_INCLUDED_
#define UTILITY_UNNAMED_PIPE_H_INCLUDED_

#include <unistd.h>
#include <cerrno>
#include <fcntl.h>

#include "../system_exception.h"
#include "fd_pair.h"
#include "fd_blocking_mode.h"

namespace utility
{
namespace posix
{

inline
fd_pair unnamed_pipe(bool in_nonblock = false, bool out_nonblock = false)
{
    int pfd[2];
    if(::pipe(pfd) < 0)
    {
        throw system_exception(errno);
    }
    fd_pair fdpair(pfd);

    if(in_nonblock && !set_fd_blocking(fdpair.in(), false))
    {
        throw system_exception(errno);
    }
    if(out_nonblock && !set_fd_blocking(fdpair.out(), false))
    {
        throw system_exception(errno);
    }
    return fdpair;
}

} // namespace posix
} // namespace utility

#endif // UTILITY_UNNAMED_PIPE_H_INCLUDED_
