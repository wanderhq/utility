#ifndef UTILITY_POSIX_PROCESS_RUNNER_H_INCLUDED_
#define UTILITY_POSIX_PROCESS_RUNNER_H_INCLUDED_

#include "../system_exception.h"
#include "../noncopyable.h"

#include "../low_level_buffer.h"
#include "../low_level_field_ref.h"

#include "unnamed_pipe.h"

#include <unistd.h>
#include <poll.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>

#if defined(PLATFORM_CYGWIN)
#include <time.h>
#endif

#include <cstdio>
#include <vector>

namespace utility
{
namespace posix
{

class process_runner
    : noncopyable
{
    enum command_packet_fields
    {
        p_type
      , p_size
      , p_args
    };
    typedef low_level_buffer<
        uint8_t     // p_type
      , uint16_t[2] // p_size
      , char[]      // p_data
    > command_packet_t;

    enum feedback_packet_fields
    {
        p_status
      , p_data
      , p_code
    };
    typedef low_level_buffer<
        uint8_t  // p_status
      , uint32_t // p_data
      , uint32_t // p_code
    > feedback_packet_t;

public:
    enum command_type
    {
        process_exit = 0xFF
      , process_run  = 0xDD
    };
    enum FeedbackStatus
    {
        R_SYS_ERROR_ = 1
     ,  R_DAT_ERROR_ = 2
     ,  R_HUP_ERROR_ = 3

     ,  R_COM_START_ = 101
     ,  R_COM_STOP_  = 102
     ,  R_COM_EXIT_  = 103
     ,  R_COM_SIGN_  = 104
     ,  R_COM_CONT_  = 105
    };

    process_runner(::timespec const & t = ::timespec())
        : m_timeout(t)
    {
        {
            fd_pair ch1 = unnamed_pipe(true); // ch parent -> child  (ch1.in() - read fd, ch1.out() - write fd)
            fd_pair ch2 = unnamed_pipe(true); // ch child  -> parent (ch2.in() - read fd, ch2.out() - write fd)

            m_pid = ::fork();
            if(m_pid < 0)
            {
                throw system_exception(errno);
            }
            else if(m_pid > 0) // parent
            {
                m_channel.reset(ch1.release_in(), ch2.release_out());
                return;
            }
            else // child
            {
                m_channel.reset(ch2.release_in(), ch1.release_out());
            }
        }
        ::_exit(monitor());
    }
    ~process_runner()
    {
        if(m_pid > 0)
        {
            uint8_t const type = process_exit;
            ::write(m_channel.out(), &type, sizeof(type));

            int status;
            int const st = ::waitpid(m_pid, &status, 0);
            assert(st != -1);
            (void)st;
        }
    }

    bool run(char const * name)
    {
        uint16_t const count = std::strlen(name) + 1;

        command_packet_t packet(count, command_packet_t::at_least());

        packet.set<p_type>(process_run);
        packet.set<p_args>(name, count);
        packet.insert<p_size>(0, count);
        packet.insert<p_size>(1, 1);

        size_t const l = ::write(m_channel.out(), packet.bytes(), packet.size());
        return (l == packet.size());
    }
    bool run(size_t argc, char const * argv[])
    {
        if(!argc)
        {
            return false;
        }
        uint16_t count = std::strlen(argv[0]) + 1;

        command_packet_t packet(count * argc, command_packet_t::at_least());

        packet.set<p_args>(argv[0], count);
        for(size_t i = 1; i < argc; ++i)
        {
            uint16_t const len = std::strlen(argv[i]) + 1;

            packet.insert<p_args>(count, argv[i], len);
            count += len;
        }
        packet.set<p_type>(process_run);
        packet.insert<p_size>(0, count);
        packet.insert<p_size>(1, argc);

        size_t const l = ::write(m_channel.out(), packet.bytes(), packet.size());
        return (l == packet.size());
    }
    FeedbackStatus get_feedback(uint32_t & data, uint32_t & code) const
    {
        feedback_packet_t fpacket;
        int const st = ::read(m_channel.in(), fpacket.bytes(), fpacket.size());
        if(st < 0)
        {
            data = errno;
            code = 0;
            return R_SYS_ERROR_;
        }
        else if(size_t(st) == fpacket.size())
        {
            fpacket.get<p_data>(data);
            fpacket.get<p_code>(code);

            uint8_t ret;
            fpacket.get<p_status>(ret);
            return static_cast<FeedbackStatus>(ret);
        }
        return FeedbackStatus();
    }

    int pin() const
    {
        return m_channel.in();
    }

private:
    void send_feedback(FeedbackStatus status, uint32_t data = 0, uint32_t code = 0) const
    {
        feedback_packet_t packet;

        packet.set<p_status>(status);
        packet.set<p_data>(data);
        packet.set<p_code>(code);

        int const st = ::write(m_channel.out(), packet.bytes(), packet.size());
        if(st < 0)
        {
            ::perror("process_runner::send_feedback");
        }
    }

    template <size_t I>
    bool read_field(command_packet_t::field_ref<I> field) const
    {
        int const st = ::read(m_channel.in(), field.bytes(), field.size());
        if(st < 0)
        {
            send_feedback(R_SYS_ERROR_, errno);
            return false;
        }
        else if(size_t(st) != field.size())
        {
            send_feedback(R_DAT_ERROR_);
            return false;
        }
        return true;
    }

    static void sigchld_dfl(int) { }

    int fork2_main(char * const args[])
    {
    #if !defined(PLATFORM_CYGWIN)
        ::sigset_t mask;

        ::signal(SIGCHLD, sigchld_dfl);
        ::sigemptyset(&mask);
        ::sigaddset(&mask, SIGCHLD);

        if(::sigprocmask(SIG_BLOCK, &mask, NULL) < 0)
        {
            send_feedback(R_SYS_ERROR_, errno);
            return 0;
        }
    #endif
        ::pid_t pid = ::fork();
        if(pid < 0) // error on fork
        {
            send_feedback(R_SYS_ERROR_, errno);
        }
        else if(pid > 0) // second fork started successfully
        {
        #if !defined(PLATFORM_CYGWIN)
            while(::sigtimedwait(&mask, NULL, &m_timeout) < 0)
        #else
            ::timespec rem;
            while(::nanosleep(&m_timeout, &rem) < 0)
        #endif
            {
                if(errno == EINTR)
                {
                    continue;
                }
                else if(errno != EAGAIN)
                {
                    send_feedback(R_SYS_ERROR_, errno);
                }
                break;
            }
            int status;
            int const chk = ::waitpid(pid, &status, WNOHANG | WUNTRACED);
            if(chk == 0)
            {
                send_feedback(R_COM_START_, pid);
            }
            else if(chk == pid)
            {
                if(WIFEXITED(status))
                {
                    send_feedback(R_COM_EXIT_, pid, WEXITSTATUS(status));
                }
                else if(WIFSIGNALED(status))
                {
                    send_feedback(R_COM_SIGN_, pid, WTERMSIG(status));
                }
                else if(WIFSTOPPED(status))
                {
                    send_feedback(R_COM_STOP_, pid, WSTOPSIG(status));
                }
                else if(WIFCONTINUED(status))
                {
                    send_feedback(R_COM_CONT_, pid);
                }
            }
            else if(chk < 0)
            {
                send_feedback(R_SYS_ERROR_, errno);
            }
        }
        else
        {
            m_channel.reset();
        #if !defined(PLATFORM_CYGWIN)
            int const r = ::sigprocmask(SIG_UNBLOCK, &mask, NULL);
            assert(r == 0);
            (void)r;
        #endif

            ::signal(SIGHUP, SIG_IGN);
            ::execvp(args[0], args);
            return -1;
        }
        return 0;
    }

    void fork2_execute(char * const argv[])
    {
        ::pid_t pid = ::fork();
        if(pid < 0)
        {
            send_feedback(R_SYS_ERROR_, errno);
        }
        else if(pid > 0)
        {
            int status;
            if(::waitpid(pid, &status, 0) < 0)
            {
                send_feedback(R_SYS_ERROR_, errno);
            }
        }
        else // pid == 0
        {
            ::_exit(fork2_main(argv));
        }
    }

    void run_command(command_packet_t & packet)
    {
        command_packet_t::field_ref<p_size> pSize(packet.get_field<p_size>());
        command_packet_t::field_ref<p_args> pData(packet.get_field<p_args>());

        if(read_field(pSize))
        {
            size_t const argc = pSize[1];
            pData.set_count(pSize[0]);
            if(read_field(pData))
            {
                char * data  = pData.bytes();
                char ** argv = new char *[argc + 1];
                for(size_t i = 0, start = 0, v = 0; v < argc; ++i)
                {
                    if(data[i] == '\0')
                    {
                        argv[v] = data + start;
                        start = i + 1;
                        ++v;
                    }
                }
                argv[argc] = 0;

                fork2_execute(argv);
                delete[] argv;
            }
        }
    }
    bool on_poll_in()
    {
        command_packet_t packet;

        command_packet_t::field_ref<p_type> pType(packet.get_field<p_type>());

        if(read_field(pType))
        {
            switch(pType.get_unsafe())
            {
            case process_exit:
                return false;

            case process_run:
                run_command(packet);
                break;

            default:
                ;
            }
        }
        return true;
    }
    bool on_poll_hup()
    {
        send_feedback(R_HUP_ERROR_);
        return false;
    }
    bool on_poll_err()
    {
        send_feedback(R_SYS_ERROR_, errno);
        return true;
    }

    int monitor() try
    {
        struct ::pollfd pfd =
        {
            m_channel.in(), POLLIN, 0
        };
        bool is_exit = false;
        while(!is_exit)
        {
            int const st = ::poll(&pfd, 1, -1);
            if(st > 0)
            {
                if(pfd.revents & POLLIN)
                {
                    is_exit = !on_poll_in();
                }
                if(pfd.revents & POLLHUP)
                {
                    is_exit = !on_poll_hup();
                }
                if(pfd.revents & POLLERR)
                {
                    is_exit = !on_poll_err();
                }
            }
            else if(st < 0)
            {
                send_feedback(R_SYS_ERROR_, errno);
                is_exit = true;
            }
        }
        return 0;
    }
    catch(std::exception const & e)
    {
        return 1;
    }

private:
    ::pid_t m_pid;
    fd_pair m_channel;

    ::timespec const m_timeout;
};

} // namespace posix
} // namespace utility

#endif // UTILITY_POSIX_PROCESS_RUNNER_H_INCLUDED_
