#ifndef UTILITY_FD_PAIR_H_INCLUDED_
#define UTILITY_FD_PAIR_H_INCLUDED_

#include "../safe_bool.h"
#include "../move.h"

#include <unistd.h>
#include <utility>

namespace utility
{
namespace posix
{

class fd_pair
    : public safe_bool<fd_pair>
{
private:
    std::pair<int, int> release()
    {
        std::pair<int, int> tmp(m_pair);
        m_pair.first = m_pair.second = -1;
        return tmp;
    }
    static void close_fd(int fd)
    {
        if(fd != -1)
        {
            ::close(fd);
        }
    }

public:
    fd_pair(int in = -1, int out = -1)
        : m_pair(in, out)
    { }
    fd_pair(int const (& p)[2])
        : m_pair(p[0], p[1])
    { }

    operator rv<fd_pair>()
    {
        return rv<fd_pair>(*this);
    }
    fd_pair(rv<fd_pair> r)
        : m_pair(r->release())
    { }

    ~fd_pair()
    {
        close_fd(m_pair.first);
        close_fd(m_pair.second);
    }

    int in() const
    {
        return m_pair.first;
    }
    int out() const
    {
        return m_pair.second;
    }

    int release_in()
    {
        int tmp = m_pair.first;
        m_pair.first = -1;
        return tmp;
    }
    int release_out()
    {
        int tmp = m_pair.second;
        m_pair.second = -1;
        return tmp;
    }

    bool operator_bool() const
    {
        return (m_pair.first != -1 && m_pair.second != -1);
    }

    void reset(int in, int out)
    {
        fd_pair(in, out).swap(*this);
    }
    void reset(int const (& p)[2])
    {
        fd_pair(p).swap(*this);
    }
    void reset()
    {
        reset_in();
        reset_out();
    }

    void reset_in(int in = -1)
    {
        std::swap(in, m_pair.first);
        close_fd(in);
    }
    void reset_out(int out = -1)
    {
        std::swap(out, m_pair.second);
        close_fd(out);
    }

    void swap(fd_pair & other)
    {
        std::swap(other.m_pair, m_pair);
    }

private:
    std::pair<int, int> m_pair;
};

} // namespace posix
} // namespace utility

#endif // UTILITY_FD_PAIR_H_INCLUDED_
