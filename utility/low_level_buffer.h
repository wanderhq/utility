#ifndef UTILITY_LOW_LEVEL_BUFFER_H_INCLUDED_
#define UTILITY_LOW_LEVEL_BUFFER_H_INCLUDED_

#include "low_level_buffer/low_level_storage.h"

#include "ct/enable_if.h"

namespace utility
{

template <typename Fields>
class low_level_buffer_aux
    : low_level_storage<Fields>
{
    typedef low_level_storage<Fields> low_level_storage_;

    template <size_t I, size_t C, typename H>
    struct find_field;

    template <size_t I, size_t C, typename T, typename Self, typename Prev>
    struct find_field<I, C, low_level_storage_composer<T, Self, Prev> >
        : find_field<I, C - 1, typename Prev::composer>
    { };

    template <size_t I, typename T, typename Self, typename Prev>
    struct find_field<I, I, low_level_storage_composer<T, Self, Prev> >
    {
        typedef low_level_field<T, Self, Prev> type;
        typedef typename type::value_type      value_type;
        typedef T                              field_type;
    };

    template <size_t I>
    struct find_field_t
        : find_field<
             I + 1
           , ct::tl_size<Fields>::value
           , typename low_level_storage_::composer
          >
    { };

    template <size_t I, size_t N, typename F = typename find_field_t<I>::type>
    struct check_array_assignment_t
        : ct::enable_if<
             (F::is_dynamic || N <= F::static_count)
          >
    { };

    template <size_t>
    struct base_index
    {
        base_index(size_t i)
            : m_i(i)
        { }
        base_index & operator++()
        {
            return ++m_i, *this;
        }
        base_index operator++(int)
        {
            return base_index(m_i++);
        }
        operator size_t() const
        {
            return m_i;
        }
        base_index & operator=(size_t i)
        {
            return (m_i = i), *this;
        }

    private:
        size_t m_i;
    };

public:
    template <size_t I, typename = low_level_buffer_aux<Fields> >
    class field_ref;
    template <size_t I>
    class const_field_ref;

    typedef base_index<0> network_index;
    typedef base_index<1> index;

private:
    template <size_t I>
    typename find_field_t<I>::type & field()
    {
        return *this;
    }
    template <size_t I>
    typename find_field_t<I>::type const & field() const
    {
        return *this;
    }

public:
    typedef low_level_buffer_aux<Fields> low_level_buffer_aux_;

    struct at_least
    { };

    explicit low_level_buffer_aux(size_t capacity)
        : low_level_storage_(capacity)
    { }
    low_level_buffer_aux(size_t capacity, at_least)
        : low_level_storage_(low_level_storage_::static_capacity + capacity)
    { }
    low_level_buffer_aux()
        : low_level_storage_()
    { }

    using low_level_storage_::size;
    using low_level_storage_::bytes;
    using low_level_storage_::capacity;
    using low_level_storage_::raw_memory;
    using low_level_storage_::zero_memory;
    using low_level_storage_::reserve;

    size_t fields() const
    {
        return ct::tl_size<Fields>::value;
    }

    template <size_t I>
    void set(typename find_field_t<I>::value_type const & val)
    {
        field<I>().set(val);
    }
    template <size_t I>
    void set(typename find_field_t<I>::value_type const * p, size_t count)
    {
        field<I>().set(p, count);
    }
    template <size_t I, size_t N>
    typename check_array_assignment_t<I, N>::type
        set(typename find_field_t<I>::value_type const (& val)[N])
    {
        field<I>().set(val);
    }

    template <size_t I>
    void insert(size_t idx, typename find_field_t<I>::value_type const * p, size_t count)
    {
        field<I>().insert(idx, p, count);
    }
    template <size_t I, size_t N>
    typename check_array_assignment_t<I, N>::type
         insert(size_t idx, typename find_field_t<I>::value_type const (& val)[N])
    {
        field<I>().insert(idx, val);
    }
    template <size_t I>
    void insert(size_t idx, typename find_field_t<I>::value_type const & val)
    {
        field<I>().insert(idx, val);
    }

    template <size_t I>
    std::pair<typename find_field_t<I>::value_type const *, size_t> get() const
    {
        typename find_field_t<I>::type const & xt = *this;
        return std::make_pair(xt.get(), xt.count());
    }
    template <size_t I, size_t N>
    typename check_array_assignment_t<I, N>::type
        get(typename find_field_t<I>::value_type (& val)[N]) const
    {
        field<I>().copy(val);
    }
    template <size_t I>
    void get(typename find_field_t<I>::value_type & val) const
    {
        field<I>().copy(val);
    }
    template <size_t I>
    typename find_field_t<I>::field_type const & get_unsafe() const
    {
        return field<I>().restore();
    }

    template <size_t I>
    void set_count(size_t count)
    {
        field<I>().set_count(count);
    }
    template <size_t I>
    size_t count() const
    {
        return field<I>().count();
    }
    template <size_t I>
    size_t size() const
    {
        return field<I>().size_of();
    }

    template <size_t I>
    void zero_memory(size_t const off = 0)
    {
        field<I>().zero_memory(off);
    }
    template <size_t I>
    void copy_memory(void const * m, size_t size, size_t const off = 0)
    {
        field<I>().copy_memory(off, m, size);
    }
    template <size_t I>
    void move_memory(void const * m, size_t size, size_t const off = 0)
    {
        field<I>().move_memory(off, m, size);
    }
    template <size_t I>
    void fill_memory(unsigned char byte, size_t size, size_t const off = 0)
    {
        field<I>().fill_memory(off, byte, size);
    }
    template <size_t I>
    int compare_memory(void const * m, size_t size, size_t const off = 0) const
    {
        return field<I>().compare_memory(off, m, size);
    }

    template <size_t I>
    void * raw_memory()
    {
        return field<I>().raw_memory();
    }
    template <size_t I>
    void const * raw_memory() const
    {
        return field<I>().raw_memory();
    }

    template <size_t I>
    char const * bytes() const
    {
        return static_cast<char const *>(field<I>().raw_memory());
    }
    template <size_t I>
    char * bytes()
    {
        return static_cast<char *>(field<I>().raw_memory());
    }
    template <size_t I>
    size_t remain() const
    {
        return this->size() - field<I>().offset();
    }

    template <size_t I>
    typename find_field_t<I>::value_type const & element_unsafe(size_t idx = 0) const
    {
        return field<I>().get()[idx];
    }
    template <size_t I>
    void element(typename find_field_t<I>::value_type & val, size_t idx = 0) const
    {
        field<I>().copy(val, idx);
    }

    template <size_t I>
    size_t is_aligned() const
    {
        return field<I>().is_aligned();
    }

    template <size_t I>
    void set_network(typename find_field_t<I>::value_type const & val)
    {
        field<I>().set_network(val);
    }
    template <size_t I, size_t N>
    typename check_array_assignment_t<I, N>::type
        set_network(typename find_field_t<I>::value_type const (& val)[N])
    {
        field<I>().set_network(val);
    }
    template <size_t I>
    void set_network(typename find_field_t<I>::value_type const * p, size_t count)
    {
        field<I>().set_network(p, count);
    }

    template <size_t I, size_t N>
    typename check_array_assignment_t<I, N>::type
        insert_network(size_t idx, typename find_field_t<I>::value_type const (& val)[N])
    {
        field<I>().insert_network(idx, val);
    }
    template <size_t I>
    void insert_network(size_t idx, typename find_field_t<I>::value_type const & val)
    {
        field<I>().insert_network(idx, val);
    }
    template <size_t I>
    void insert_network(size_t idx, typename find_field_t<I>::value_type const * p, size_t count)
    {
        field<I>().insert_network(idx, p, count);
    }

    template <size_t I>
    void get_network(typename find_field_t<I>::value_type & val) const
    {
        field<I>().copy_network(val);
    }
    template <size_t I, size_t N>
    typename check_array_assignment_t<I, N>::type
        get_network(typename find_field_t<I>::value_type (& val)[N]) const
    {
        field<I>().copy_network(val);
    }
    template <size_t I>
    void element_network(typename find_field_t<I>::value_type & val, size_t idx = 0) const
    {
        field<I>().copy_network(val, idx);
    }

    template <size_t I>
    field_ref<I> get_field()
    {
        return field_ref<I>(*this);
    }
    template <size_t I>
    const_field_ref<I> get_field() const
    {
        return const_field_ref<I>(*this);
    }

    bool operator==(low_level_buffer_aux const & x) const
    {
        return this == &x || x.equal(*this);
    }
    bool operator!=(low_level_buffer_aux const & x) const
    {
        return this != &x && !x.equal(*this);
    }

    unsigned char operator[](size_t idx) const
    {
        return static_cast<unsigned char *>(this->raw_memory())[idx];
    }
    unsigned char & operator[](size_t idx)
    {
        return static_cast<unsigned char *>(this->raw_memory())[idx];
    }

    template <size_t I>
    unsigned char at(size_t idx) const
    {
        return static_cast<unsigned char *>(this->raw_memory<I>())[idx];
    }
    template <size_t I>
    unsigned char & at(size_t idx)
    {
        return static_cast<unsigned char *>(this->raw_memory<I>())[idx];
    }
};


#if __cplusplus >= 201103L // C++11

template <typename ...Args>
class low_level_buffer
    : public low_level_buffer_aux<
          typename ct::tl_reverse<
              typename ct::make_list<Args...>::type
          >::type
      >
{
public:
    using low_level_buffer::low_level_buffer_aux_::low_level_buffer_aux_;
};

#else

template <
    typename F00 = ct::null_list, typename F01 = ct::null_list
  , typename F02 = ct::null_list, typename F03 = ct::null_list
  , typename F04 = ct::null_list, typename F05 = ct::null_list
  , typename F06 = ct::null_list, typename F07 = ct::null_list
  , typename F08 = ct::null_list, typename F09 = ct::null_list
  , typename F10 = ct::null_list, typename F11 = ct::null_list
  , typename F12 = ct::null_list, typename F13 = ct::null_list
  , typename F14 = ct::null_list, typename F15 = ct::null_list
  , typename F16 = ct::null_list, typename F17 = ct::null_list
  , typename F18 = ct::null_list, typename F19 = ct::null_list
  , typename F20 = ct::null_list
>
class low_level_buffer
    : public low_level_buffer_aux<
          typename ct::tl_reverse<
              typename ct::make_list<
                  F00, F01, F02, F03, F04, F05, F06, F07, F08, F09
               ,  F10, F11, F12, F13, F14, F15, F16, F17, F18, F19
               ,  F20
              >::type
          >::type
      >
{
public:
    explicit low_level_buffer(size_t capacity)
        : low_level_buffer::low_level_buffer_aux_(capacity)
    { }
    low_level_buffer(size_t capacity, typename low_level_buffer::at_least x)
        : low_level_buffer::low_level_buffer_aux_(capacity, x)
    { }
    low_level_buffer()
        : low_level_buffer::low_level_buffer_aux_()
    { }
};
#endif

} // namespace utility

#endif // UTILITY_LOW_LEVEL_BUFFER_H_INCLUDED_
