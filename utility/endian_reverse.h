#ifndef UTILITY_ENDIAN_REVERSE_H_INCLUDED_
#define UTILITY_ENDIAN_REVERSE_H_INCLUDED_

#include "platform_config.h"

#ifdef PLATFORM_WIN32
#define __BYTE_ORDER    1
#define __LITTLE_ENDIAN 1
#else
#ifdef PLATFORM_BSD
#include <sys/endian.h>
#else
#include <endian.h>
#endif
#endif

#include <cassert>
#include <cstddef>
#include <cstring>

namespace utility
{
namespace detail
{

template <size_t Byte>
struct bytes_rev_
{
    static
    void reverse(char * dest, char const * src)
    {
        dest[Byte] = *src;
        bytes_rev_<Byte - 1>::reverse(dest, src + 1);
    }
};
template <>
struct bytes_rev_<0>
{
    static
    void reverse(char * dest, char const * src)
    {
        dest[0] = *src;
    }
};

template <size_t BlockSize>
struct endian_traits
{
    static
    void reverse(void * dest, void const * src, size_t size)
    {
        assert(size % BlockSize == 0); // shall be multiple of BlockSize

        char * start  = static_cast<char *>(dest);
        char * finish = start + size;

        char const * bytes = static_cast<char const *>(src);
        while(start != finish)
        {
            bytes_rev_<BlockSize - 1>::reverse(start, bytes);
            start += BlockSize;
            bytes += BlockSize;
        }
    }
};

} // namespace detail

template <size_t BlockSize>
void endian_reverse(void * dest, void const * src, size_t size)
{
#if   __BYTE_ORDER == __BIG_ENDIAN
    std::memcpy(dest, src, size);
#elif __BYTE_ORDER == __LITTLE_ENDIAN
    detail::endian_traits<BlockSize>::reverse(dest, src, size);
#endif
}

template <>
inline
void endian_reverse<1>(void * dest, void const * src, size_t size)
{
    std::memcpy(dest, src, size);
}

} // namespace utility

#endif // UTILITY_ENDIAN_REVERSE_H_INCLUDED_
