#ifndef UTILITY_SYSTEM_EXCEPTION_H_INCLUDED_
#define UTILITY_SYSTEM_EXCEPTION_H_INCLUDED_

#include <exception>
#include <cstring>

namespace utility
{

class system_exception
    : public std::exception
{
public:
    explicit system_exception(int e) throw()
        : m_errno(e)
    { }

    /*virtual*/
    ~system_exception() throw()
    { }

    /*virtual*/
    char const * what() const throw()
    {
        return std::strerror(m_errno);
    }

private:
    int const m_errno;
};

} // namespace utility

#endif // UTILITY_SYSTEM_EXCEPTION_H_INCLUDED_
