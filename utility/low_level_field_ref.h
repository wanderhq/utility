#ifndef UTILITY_LOW_LEVEL_FIELD_REF_H_INCLUDED_
#define UTILITY_LOW_LEVEL_FIELD_REF_H_INCLUDED_

#include "low_level_buffer.h"

#include "ct/elements_count.h"

namespace utility
{

template <typename Fields>
template <size_t I, typename LL_Buffer_AUX>
class low_level_buffer_aux<Fields>::field_ref
{
    friend class low_level_buffer_aux<Fields>;

    typedef typename LL_Buffer_AUX::template find_field_t<I> find_field_t;

    typedef typename find_field_t::value_type value_type;
    typedef typename find_field_t::field_type field_type;

    template <size_t N>
    struct check_array_assignment_t
        : LL_Buffer_AUX::template check_array_assignment_t<I, N>
    { };

    explicit field_ref(LL_Buffer_AUX & ref)
        : m_ref(ref)
    { }

public:
    class type
    {
    public:
        type(field_ref const & field, typename LL_Buffer_AUX::index idx)
        {
            field.element(m_value, idx);
        }
        type(field_ref const & field, typename LL_Buffer_AUX::network_index idx)
        {
            field.element_network(m_value, idx);
        }
        type(field_ref const & field)
        {
            field.get(m_value);
        }
        operator value_type const &() const
        {
            return m_value;
        }
        size_t count() const
        {
            return UTILITY_ELEMENTS_COUNT(m_value);
        }

    private:
        value_type m_value;
    };

    void operator=(value_type const & val)
    {
        m_ref.template set<I>(val);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        operator=(value_type const (& val)[N])
    {
        m_ref.template set<I>(val);
    }
    void set(value_type const * p, size_t count)
    {
        m_ref.template set<I>(p, count);
    }
    void set(value_type const & val)
    {
        m_ref.template set<I>(val);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        set(value_type const (& val)[N])
    {
        m_ref.template set<I>(val);
    }

    void insert(size_t idx, value_type const & val)
    {
        m_ref.template insert<I>(idx, val);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        insert(size_t idx, value_type const (& val)[N])
    {
        m_ref.template insert<I>(idx, val);
    }
    void insert(size_t idx, value_type const * p, size_t count)
    {
        m_ref.template insert<I>(idx, p, count);
    }

    std::pair<value_type const *, size_t> get() const
    {
        return m_ref.template get<I>();
    }

    void get(value_type & val) const
    {
        m_ref.template get<I>(val);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        get(value_type (& val)[N]) const
    {
        m_ref.template get<I>(val);
    }

    field_type const & get_unsafe() const
    {
        return m_ref.template get_unsafe<I>();
    }

    void set_count(size_t count)
    {
        m_ref.template set_count<I>(count);
    }
    size_t count() const
    {
        return m_ref.template count<I>();
    }
    size_t size() const
    {
        return m_ref.template size<I>();
    }

    value_type const & element_unsafe(size_t idx = 0) const
    {
        return m_ref.template element_unsafe<I>(idx);
    }
    void element(value_type & val, size_t idx = 0) const
    {
        m_ref.template element<I>(val, idx);
    }
    void element_network(value_type & val, size_t idx = 0) const
    {
        m_ref.template element_network<I>(val, idx);
    }

    size_t is_aligned() const
    {
        return m_ref.template is_aligned<I>();
    }

    void set_network(value_type const & val)
    {
        m_ref.template set_network<I>(val);
    }
    void set_network(value_type const * p, size_t size)
    {
        m_ref.template set_network<I>(p, size);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        set_network(value_type const (& val)[N])
    {
        m_ref.template set_network<I>(val);
    }

    void insert_network(size_t idx, value_type const & val)
    {
        m_ref.template insert_network<I>(idx, val);
    }
    void insert_network(size_t idx, value_type const * p, size_t size)
    {
        m_ref.template insert_network<I>(idx, p, size);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        insert_network(size_t idx, value_type const (& val)[N])
    {
        m_ref.template insert_network<I>(idx, val);
    }

    void get_network(value_type & val) const
    {
        m_ref.template get_network<I>(val);
    }
    template <size_t N>
    typename check_array_assignment_t<N>::type
        get_network(value_type (& val)[N]) const
    {
        m_ref.template get_network<I>(val);
    }

    type operator[](typename LL_Buffer_AUX::index idx) const
    {
        return type(*this, idx);
    }
    type operator[](typename LL_Buffer_AUX::network_index idx) const
    {
        return type(*this, idx);
    }
    type operator[](size_t idx) const
    {
        return type(*this, typename LL_Buffer_AUX::index(idx));
    }

    void copy_memory(void const * m, size_t size, size_t const off = 0)
    {
        m_ref.template copy_memory<I>(m, size, off);
    }
    void move_memory(void const * m, size_t size, size_t const off = 0)
    {
        m_ref.template move_memory<I>(m, size, off);
    }
    void fill_memory(unsigned char byte, size_t size, size_t const off = 0)
    {
        m_ref.template fill_memory<I>(byte, size, off);
    }
    void zero_memory()
    {
        m_ref.template zero_memory<I>();
    }
    int compare_memory(void const * m, size_t size, size_t const off = 0) const
    {
        return m_ref.template compare_memory<I>(m, size, off);
    }

    void * raw_memory()
    {
        return m_ref.template raw_memory<I>();
    }
    void const * raw_memory() const
    {
        return m_ref.template raw_memory<I>();
    }

    char const * bytes() const
    {
        return m_ref.template bytes<I>();
    }
    char * bytes() 
    {
        return m_ref.template bytes<I>();
    }
    size_t remain() const
    {
        return m_ref.template remain<I>();
    }

private:
    LL_Buffer_AUX & m_ref;
};

template <typename Fields>
template <size_t I>
class low_level_buffer_aux<Fields>::const_field_ref
    : public field_ref<I, low_level_buffer_aux<Fields> const>
{
    friend class low_level_buffer_aux<Fields>;

    typedef low_level_buffer_aux<Fields> const LL_Buffer_AUX;

    explicit const_field_ref(LL_Buffer_AUX & ref)
        : field_ref<I, LL_Buffer_AUX>(ref)
    { }

public:
    const_field_ref(field_ref<I> const & ref)
        : field_ref<I, LL_Buffer_AUX>(ref.m_ref)
    { }

    using field_ref<I, LL_Buffer_AUX>::operator=;
};

template <size_t I, typename F>
struct low_level_field_ref
    : F::template field_ref<I>
{
    typedef typename F::template field_ref<I> field_ref_t;

    low_level_field_ref(field_ref_t const & x)
        : field_ref_t(x)
    { }

    using field_ref_t::operator=;
};

template <size_t I, typename F>
struct low_level_const_field_ref
    : F::template const_field_ref<I>
{
    typedef typename F::template const_field_ref<I> const_field_ref_t;
    typedef typename F::template field_ref<I>       field_ref_t;

    low_level_const_field_ref(const_field_ref_t const & x)
        : const_field_ref_t(x)
    { }
    low_level_const_field_ref(field_ref_t const & x)
        : const_field_ref_t(x)
    { }

    using const_field_ref_t::operator=;
};

} // namespace utility

#endif // UTILITY_LOW_LEVEL_FIELD_REF_H_INCLUDED_
