#ifndef UTILITY_ALIGNED_TYPE_H_INCLUDED_
#define UTILITY_ALIGNED_TYPE_H_INCLUDED_

#include "align_as.h"
#include "aligned_storage.h"

namespace utility
{

template <typename T, size_t Align = ct::align_of<max_align_t>::value>
struct make_aligned
{
#if defined(UTILITY_ALIGN_AS_SUPPORTED) && (COMPILER_GCC > 401 || COMPILER_MINGW > 401)
    typedef T UTILITY_ALIGN_AS(Align)                        type;
#else
    typedef typename aligned_storage<sizeof(T), Align>::type type;
#endif
};

} // namespace utility

#endif // UTILITY_ALIGNED_TYPE_H_INCLUDED_
