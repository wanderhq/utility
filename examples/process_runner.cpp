#include "../utility/posix/process_runner.h"
#include <cstdio>
#include <limits.h>

struct command
{
    char const * comm;
    size_t       length;

    template <size_t N>
    command(char const (&c)[N])
        : comm(c), length(N)
    { }

    char const * check(char const * buf, size_t len) const
    {
        if(std::memcmp(buf, comm, std::min(length - 1, len)) == 0)
        {
            return buf + length;
        }
        return 0;
    }
};

void runner_feedback(utility::posix::process_runner & runner)
{
    namespace up = utility::posix;

    uint32_t data, code;

    up::process_runner::FeedbackStatus status = runner.get_feedback(data, code);

    switch(status)
    {
    case up::process_runner::R_COM_START_:
        std::printf(" >>[STARTED] with PID: [%d]\n", data);
        break;
    case up::process_runner::R_COM_EXIT_:
        if(code == 255)
        {
            std::printf(" >>[FAILED], was PID: [%d]\n", data);
        }
        else
        {
            std::printf(" >>[EXITED], was PID: [%d], EXIT CODE: [%d]\n", data, code);
        }
        break;
    case up::process_runner::R_COM_SIGN_:
        std::printf(" >>[EXITED BY SIGNAL], was PID: [%d], SIGNAL: [%s]\n", data, strsignal(code));
        break;
    case up::process_runner::R_SYS_ERROR_:
        std::printf(" >>[SYS ERROR]: [%s]\n", strerror(data));
        break;
    case up::process_runner::R_DAT_ERROR_:
        std::printf(" >>[DATA TRANSFER ERROR]\n");
        break;
    default:
        ;
    }
}

bool runner_command(int fd, utility::posix::process_runner & runner)
{
    static const command commands[] =
    {
        "exit", "run", "kill"
    };

    char buf[PATH_MAX];
    int const st = ::read(fd, buf, sizeof(buf) - 1);
    if(st > 0)
    {
        buf[st - (buf[st - 1] == '\n')] = 0;

        if(commands[0].check(buf, st))
        {
            return true;
        }
        else if(char const * comm_arg = commands[1].check(buf, st))
        {
            if(runner.run(comm_arg))
            {
                std::printf(" > running %s...\n", comm_arg);
            }
        }
        else if(char const * comm_arg = commands[2].check(buf, st))
        {
            int const pid = std::atoi(comm_arg);
            if(pid)
            {
                std::printf(" > killing %s...\n", comm_arg);
                ::kill(pid, SIGTERM);
            }
        }
    }
    return false;
}

int main()
{
    namespace up = utility::posix;

    ::timespec t = {3, 0};
    up::process_runner runner(t);

    struct ::pollfd mon[] =
    {
        { fileno(stdin), POLLIN, 0 }
      , { runner.pin(),  POLLIN, 0 }
    };
    bool is_exit = false;
    while(!is_exit)
    {
        int const st = ::poll(mon, 2, -1);
        if(st > 0)
        {
            if(mon[0].revents & POLLIN)
            {
                is_exit = runner_command(mon[0].fd, runner);
            }
            if(mon[1].revents & POLLIN)
            {
                runner_feedback(runner);
            }
        }
        else if(st < 0)
        {
            is_exit = true;
            ::perror("runner");
        }
    }
}
